--> Outsourced!
local initializeRenderState = function()
	local render = {}
	render.visibleHexagons = {} --> Contains all hexagons that are visible at the current frame (as in, on-screen)!
	render.hexagonCanvas = love.graphics.newCanvas(sim.hexagonWidth + 2, sim.hexagonHeight + 2) --> Make it a bit bigger to ensure correct edge drawing behaviour!
	render.hexagonCanvas:renderTo(function()
		love.graphics.clear(0, 0, 0, 0)
		love.graphics.setColor(255, 255, 255, 255)
		love.graphics.circle("fill", 0.5 * render.hexagonCanvas:getWidth(), 0.5 * render.hexagonCanvas:getHeight(), 0.5 * sim.hexagonWidth + 1, 6)
	end)
	render.grassSpriteBatch = love.graphics.newSpriteBatch(render.hexagonCanvas, 1, "dynamic") --> Normal tile spriteBatch for grass!
	render.beachSpriteBatch = love.graphics.newSpriteBatch(render.hexagonCanvas, 1, "dynamic") --> Normal tile spriteBatch for beaches!
	render.waterSpriteBatch = love.graphics.newSpriteBatch(render.hexagonCanvas, 1, "dynamic") --> Normal tile spriteBatch for plain water!
	render.dotCanvas = {} --> Not one canvas but 10!
	for i=1,10,1 do
		local newCanvas = love.graphics.newCanvas(sim.hexagonWidth, sim.hexagonHeight)
		table.insert(render.dotCanvas, newCanvas)
		for i=1,love.math.random(10, 15),1 do --> Number of dots is random!
			local dotSize = 8
			local sizeFactor = 2
			if love.math.random(1, 5) == 1 then
				sizeFactor = 1
			end
			local colorFactor = math.rsign() --> -1 or 1!
			local x, y = love.math.random(0, newCanvas:getWidth() - 1), love.math.random(0, newCanvas:getHeight() - 1)
			--> Check if new point is on hex with all corners!
			if _hexagon.isPointInside(sim.hexagonWidth/2, sim.hexagonHeight/2, sim.hexagonWidth, x, y) then
				if _hexagon.isPointInside(sim.hexagonWidth/2, sim.hexagonHeight/2, sim.hexagonWidth, x + dotSize/sizeFactor, y + dotSize/sizeFactor) then
					if _hexagon.isPointInside(sim.hexagonWidth/2, sim.hexagonHeight/2, sim.hexagonWidth, x, y + dotSize/sizeFactor) then
						if _hexagon.isPointInside(sim.hexagonWidth/2, sim.hexagonHeight/2, sim.hexagonWidth, x + dotSize/sizeFactor, y) then
							newCanvas:renderTo(function()
								if colorFactor == 1 then
									love.graphics.setColor(255, 255, 255, 25)
								else
									love.graphics.setColor(0, 0, 0, 25)
								end
								love.graphics.rectangle("fill", x, y, dotSize/sizeFactor, dotSize/sizeFactor)
							end)
						end
					end
				end
			end
		end
	end
	render.dotSpriteBatch = {} --> Not one spriteBatch but 10!
	for i=1,#render.dotCanvas,1 do
		table.insert(render.dotSpriteBatch, love.graphics.newSpriteBatch(render.dotCanvas[i], 1, "dynamic"))
	end
	render.hexRenderInfo = {} --> Contains a mirror. Each hexagon in sim.map.hexagons has an entry here at the same index that contains additional render information about that hexagon!
	render.mountainMesh = nil --> Important to notice that this contains the mesh for the mountains!
	render.meshPoints = {}
	render.whitemap = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight(), "normal", 0) --> For fog of war!
	--> Images!
	render.images = {}
	render.images.grassLookup = love.image.newImageData("grassLookup.png") --> For mapping elevation to color!
	render.images.waves = love.graphics.newImage("waves.png") --> For waves animation!
	--> Animation anim8 library variables (grids and animations)!
	render.anim8 = {}
	render.anim8.grids = {}
	render.anim8.animations = {}
	render.anim8.animationCoordinates = {}
	render.anim8.animationCoordinates.water = {}
	render.anim8.grids.waterGrid = _anim8.newGrid(render.images.waves:getWidth()/60, render.images.waves:getHeight(), render.images.waves:getWidth(), render.images.waves:getHeight())
	render.anim8.animations.waterAnimation = _anim8.newAnimation(render.anim8.grids.waterGrid('1-60', 1), 1/15)
	--> Units
	render.units = {} --> Mirror of sim.units table containing additional render information!
	render.particleSystemCanvas = love.graphics.newCanvas(8, 8)
	render.particleSystemCanvas:renderTo(function()
		love.graphics.clear(0, 0, 0, 0)
		love.graphics.setColor(255, 255, 255, 255)
		love.graphics.rectangle("fill", 0, 0, 8, 8)
	end)
	--> Create effects for fog of war!
	_effect:_nebula("nebula", "none", true, false, 1, {rc = 0.1, gc = 0.1, bc = 0.1, a = 1, tm = 0.2, scale = 100}) --> Create effects for fog of war!
	_effect:_blur("fovBlur", "Blur for player fov", false, false, 0, {samples = 3, downScaleFactor = 4}) --> Create effects for fog of war!
	return render
end

local updateKeyboardScrolling = function(dt)
		
	local m = {x = 0, y = 0}
	if love.keyboard.isDown("w", "up") then
		m.y = m.y - 1
	end
	if love.keyboard.isDown("s", "down") then
		m.y = m.y + 1
	end
	if love.keyboard.isDown("a", "left") then
		m.x = m.x - 1
	end
	if love.keyboard.isDown("d", "right") then
		m.x = m.x + 1
	end
	local oldX, oldY = state.camera:getPosition()
	state.camera:setPosition(oldX + m.x * dt * 1000, oldY + m.y * dt * 1000)

end

local _gamestate = function()

	--> Make a new gamestate!
	local state = {}
	
	--> Gamera object!
	state.camera = _gamera.new(0, 0, 1, 1)
	
	--> This table contains more or less everything (all data) that is needed to render the world!
	state.render = initializeRenderState()

	--> Informationary vaiables!
	state.selection = {dragMoved = false} --> Holds data needed by the _selection class, also some relevant variables!
	
	--> Updates the gamestate!
	state.update = function(dt)
	
		--> First work over all the updates on the sim out stack!
		while true do --> While there are updates on the sim out stack, proceed!
			local data = sim:fetch()
			if not data then
				break
			end
			--> Have a look at what kind of update it is, then apply it!
			if data.type == "joined" then --> We have to make a map render state thingy!	
				if data.content.id == sim.id then --> If it was me, who joined!
					--> Now create a new camera for the world!
					state.camera:setWorld(-0.5*love.graphics.getWidth()-((-sim.lowestColumnIndex + 1) * 0.75 * sim.hexagonWidth),
										  -0.5*love.graphics.getHeight()-((-sim.lowestRowIndex + 1) * sim.hexagonHeight),
										  sim.mapWidth+love.graphics.getWidth(),
										  sim.mapHeight+love.graphics.getHeight())
					state.camera:setPosition(data.content.position.x, data.content.position.y) --> Set camera!
					
					--> When I join, i set all the units to be rendered!
					state.render.units = {}
					for i,v in ipairs(sim.units) do
						table.insert(state.render.units, _unit.create({particleSystemCanvas = state.render.particleSystemCanvas, unit = v}))
					end
					
				else
					
					--> If someone else joined, create their unit!
					table.insert(state.render.units, _unit.create({particleSystemCanvas = state.render.particleSystemCanvas, unit = sim.units[#state.render.units + 1]}))
					
				end
			
			elseif data.type == "left" then --> Remove all unit renders by that player!
				for i=#state.render.units,1,-1 do
					if state.render.units[i].unit.owner.id == data.content.id then
						table.remove(state.render.units, i)
					end
				end		
			
			elseif data.type == "init" then --> This gets called when any player initially joines a game. It overrides the gamestate, so all renderthings have to be remade!


			elseif data.type == "unitOrder" then
		
			end
		end

		--> Update keyboard scrolling with wasd or the arrow keys!
		updateKeyboardScrolling(dt)
		
		
		--> Update _selection class!
		_selection:update(dt)
		
		--> Update all anim8 animations!
		for i,v in pairs(state.render.anim8.animations) do
			v:update(dt)
		end
		
		--> Update all unit renders!
		for i=#state.render.units,1,-1 do
		
			local v = state.render.units[i]
			
			if v.dead then
				table.remove(state.render.units, i)	
			end
			
			v:update(dt)
			
		end
		
		--> For nebula and blur (fog of war)!
		_effect:updateAll(dt)
		
	end
	
	--> Draws the gamestate!
	state.draw = function()
		if sim.map then --> World was already created!

			--> Draw everything through the camera!
			state.camera:draw(function(l, t, w, h)
			
				 --> Draw map!
				_map.draw()
				
				do --> Draw hovered hexagon, if any!
					local h = _selection:getHovered()
					if h then
						_hexagon.drawBorder(sim.map.hexagons[h.x][h.y])
					end
				end

				--> Draw all unit renders!
				for i,v in ipairs(state.render.units) do
					v:draw()
				end

			end)
				
			--> Draw selection thingy around units!
			_selection:draw()
		
			--> Draw fog of war shader!
			_map.drawShading()

		end
			
	end
	
	--> For mouse move events!
	state.mousemoved = function(x, y, dx, dy)
		_selection:mousemoved(x, y, dx, dy)
		if love.mouse.isDown(2) then --> Used for right-click scrolling!
			if state.camera then --> Connection was successfully established!
				local oldX, oldY = state.camera:getPosition()
				state.camera:setPosition(oldX - dx, oldY - dy)
			end
			state.selection.dragMoved = true
		end	
	end
	
	state.mousepressed = function(x, y, button, istouch)
		_selection:mousepressed(x, y, button, istouch)
	end
	
	state.mousereleased = function(x, y, button, istouch)
		_selection:mousereleased(x, y, button, istouch)
		local target = _selection:getHovered()
		if target and button == 2 and _selection:countSelected() ~= 0 and not state.selection.dragMoved then
			local additional = love.keyboard.isDown("rshift") or love.keyboard.isDown("lshift") --> Adds an order if shift is pressed, makes it exclusive if not!
			for i,v in pairs(_selection:getSelected()) do
				_nw:push({type = "unitOrder", content = {step = sim.step, unitIndex = v, type = "move", target = target, additional = additional}}) --> Order units to move!
			end
		end
		state.selection.dragMoved = false
	end
	
	return state
	
end
	
return _gamestate