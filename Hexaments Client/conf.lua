												function love.conf(t)
												_G.configuration = {}
											  -- __ _                       _   _
							  -- ___ ___  _ __  / _(_) __ _ _   _ _ __ __ _| |_(_) ___  _ __
							 -- / __/ _ \| '_ \| |_| |/ _` | | | | '__/ _` | __| |/ _ \| '_ \
							-- | (_| (_) | | | |  _| | (_| | |_| | | | (_| | |_| | (_) | | | |
							 -- \___\___/|_| |_|_| |_|\__, |\__,_|_|  \__,_|\__|_|\___/|_| |_|
												   -- |___/

--###################################################################################################################--
--				  					   Feel free to modify the following variables!									 --
--###################################################################################################################--

	t.window.vsync				=	false					--> Toggle vertical sync!
	t.window.msaa				=	0						--> Number of samples for multi-sampled antialiasing!
	t.window.fullscreen			=	false					--> Toggle fullscreen!
	t.window.width				=	960					--> Horizontal resolution!
	t.window.height				=	540						--> Vertical resolution!
	_G.configuration.name		=	"Aconitin"				--> Your Nickname!
	_G.configuration.server		=	"127.0.0.1:1337"		--> Game Server IP:Port!

--###################################################################################################################--
--###################################################################################################################--

								 -- _                _                   _ _   _
								-- | |__  _   _     / \   ___ ___  _ __ (_) |_(_)_ __
								-- | '_ \| | | |   / _ \ / __/ _ \| '_ \| | __| | '_ \
								-- | |_) | |_| |  / ___ \ (_| (_) | | | | | |_| | | | |
								-- |_.__/ \__, | /_/   \_\___\___/|_| |_|_|\__|_|_| |_|
									   -- |___/

--###################################################################################################################--
--												     aconitin@gmx.de						     					 --
--###################################################################################################################--
--											vvvvv Do not modify these vvvvv

												t.identity = "Hexaments"
												t.version = "0.10.1"
												t.console = true
												t.accelerometerjoystick = false
												t.gammacorrect = false
												t.window.title = "Hexaments"
												t.window.icon = "icon.png"
												t.window.borderless = false
												t.window.resizable = false
												t.window.minwidth = 1
												t.window.minheight = 1
												t.window.fullscreentype = "desktop"
												t.window.display = 1
												t.window.highdpi = false
												t.window.x = nil
												t.window.y = nil
												t.modules.audio = true
												t.modules.event = true
												t.modules.graphics = true
												t.modules.image = true
												t.modules.joystick = true
												t.modules.keyboard = true
												t.modules.math = true
												t.modules.mouse = true
												t.modules.physics = true
												t.modules.sound = true
												t.modules.system = true
												t.modules.timer = true
												t.modules.touch = true
												t.modules.video = true
												t.modules.window = true
												t.modules.thread = true

														end
