local particleStates = {

	apply = function(self, i_unit, psEffectName, parameters) --> Call this to apply a state with parameters. psEffectName == unitType .. unitStatus!
		parameters = parameters or {}
		parameters.teamColor = parameters.teamColor or {r = 0, g = 0, b = 0}
		self[psEffectName](self, i_unit.ps, parameters)
	end,

	windidle = function(self, ps, p) --> Normal wind elemental state (whirlwind)!
		ps:setAreaSpread("uniform", 0, 0)
		ps:setColors(255, 255, 255, 0, 255, 255, 255, 100, 255, 255, 255, 200, 255, 255, 255, 200, p.teamColor.r, p.teamColor.g, p.teamColor.b, 200, 255, 255, 255, 200, 255, 255, 255, 200, 255, 255, 255, 0)
		ps:setDirection(math.rad(0))
		ps:setEmissionRate(math.clamp(10, math.ceil(100 * p.hp), 100))
		ps:setEmitterLifetime(-1)
		ps:setInsertMode("bottom")
		ps:setParticleLifetime(2, 2)
		ps:setRadialAcceleration(-100, -100)
		ps:setRotation(math.rad(0), math.rad(360))
		ps:setSizes(1, 1, 0.5, 0.25, 0)
		ps:setSizeVariation(1)
		ps:setSpeed(60, 80)
		ps:setSpin(math.rad(0), math.rad(360))
		ps:setSpinVariation(1)
		ps:setSpread(math.rad(360))
		ps:setTangentialAcceleration(40, 40)
	end,

	windmoving = function(self, ps, p) --> Wind moves over the map!
		ps:setAreaSpread("uniform", 0, 0)
		ps:setColors(255, 255, 255, 0, p.teamColor.r, p.teamColor.g, p.teamColor.b, 200, 255, 255, 255, 200, 255, 255, 255, 0)
		ps:setDirection(math.rad(0))
		ps:setEmissionRate(math.clamp(20, math.ceil(200 * p.hp), 200))
		ps:setEmitterLifetime(-1)
		ps:setInsertMode("bottom")
		ps:setParticleLifetime(0.75, 0.75)
		ps:setRadialAcceleration(-100, -100)
		ps:setRotation(math.rad(0), math.rad(360))
		ps:setSizes(1, 1, 0.5, 0.25, 0)
		ps:setSizeVariation(1)
		ps:setSpeed(60, 80)
		ps:setSpin(math.rad(0), math.rad(360))
		ps:setSpinVariation(1)
		ps:setSpread(math.rad(360))
		ps:setTangentialAcceleration(40, 40)
	end,

    windcollecting = function(self, ps, p) --> Wind collects!
        self["windidle"](self, ps, p)
    end,

	wateridle = function(self, ps, p) --> Normal wind elemental state (whirlwind)!
		ps:setAreaSpread("uniform", 0, 0)
		ps:setColors(p.teamColor.r, p.teamColor.g, p.teamColor.b, 0, p.teamColor.r, p.teamColor.g, p.teamColor.b, 255, 12, 50, 120, 255, 12, 50, 120, 255, 12, 50, 120, 255, 12, 50, 120, 255, 12, 50, 120, 255, 55, 122, 246, 0)
		ps:setDirection(math.rad(90))
		ps:setEmissionRate(50)
		ps:setEmitterLifetime(-1)
		ps:setInsertMode("bottom")
		ps:setLinearAcceleration(0, 0, 0, 0)
		ps:setParticleLifetime(2.25, 2.5)
		ps:setRadialAcceleration(0, 0)
		ps:setRotation(math.rad(0), math.rad(360))
		ps:setSizes(1, 1, 0.5, 0.25, 0)
		ps:setSizeVariation(1)
		ps:setSpeed(-15, -15)
		ps:setSpin(math.rad(0), math.rad(360))
		ps:setSpinVariation(1)
		ps:setSpread(math.rad(360))
		ps:setTangentialAcceleration(1, 1)
	end,

	watermoving = function(self, ps, p) --> Normal wind elemental state (whirlwind)!
		ps:setAreaSpread("uniform", 20, 20)
		ps:setColors(p.teamColor.r, p.teamColor.g, p.teamColor.b, 0, p.teamColor.r, p.teamColor.g, p.teamColor.b, 255, 12, 50, 120, 255, 12, 50, 120, 255, 12, 50, 120, 255, 12, 50, 120, 255, 12, 50, 120, 255, 55, 122, 246, 0)
		ps:setDirection(math.rad(90))
		ps:setEmissionRate(100)
		ps:setEmitterLifetime(-1)
		ps:setInsertMode("bottom")
		ps:setLinearAcceleration(0, 0, 0, 0)
		ps:setParticleLifetime(1, 1)
		ps:setRadialAcceleration(0, 0)
		ps:setRotation(math.rad(0), math.rad(360))
		ps:setSizes(0.5, 1, 1, 0.5)
		ps:setSizeVariation(0)
		ps:setSpeed(-5, -5)
		ps:setSpin(math.rad(0), math.rad(360))
		ps:setSpinVariation(1)
		ps:setSpread(math.rad(360))
		ps:setTangentialAcceleration(1, 1)
	end,

}

local _unit = { --> Contains additional drawing info/states for the sim.units!

	create = function(p)

		--> Parameters overview!
		local p = p or {}
		p.particleSystemCanvas = p.particleSystemCanvas or nil --> Throws error when canvas is not given!
		p.bufferSize = p.bufferSize or 200
		p.unit = p.unit --> The unit in sim.units that this render corresponds to!

		--> New unit render!
		local ret = {} --> The new unit render!
		ret.ps = love.graphics.newParticleSystem(p.particleSystemCanvas, p.bufferSize)
		ret.ps:start()
		ret.unit = p.unit
		ret.selected = false --> Turns true if this unit is selected!

		--> Mirrored values. These values are copied from sim.units[i] to detect changes and trigger appropriate render responses!
		ret.dead = false --> When a sim unit dies, this triggers a final death animation!

		function ret:update(dt) --> Updates. simUnit is the sim.unit corresponding to this render!

			--> Update particle system!
			self.ps:update(dt)
			self.ps:setPosition(self.unit.x, self.unit.y)

			--> Check if the unit that is represented died!
			if not self.dead and (not self.unit or self.unit.dead) then
                self:apply(self.unit.type .. "idle", {teamColor = self.unit.owner.color, hp = 0})
				self.selected = false
				if self.ps:getCount() ~= 0 then
					self.ps:emit(self.ps:getEmissionRate())
					self.ps:stop()
				else
					self.dead = true
				end
            else
                --> Might be costly - applies all changes to ps!
                self:apply(self.unit.type .. self.unit.status, {teamColor = self.unit.owner.color, hp = self.unit.hp})
			end
		end

		function ret:draw()
			if state.selection.alted or self.selected then
				love.graphics.setColor(255, 255, 255, 150)
			else
				love.graphics.setColor(255, 255, 255, 255)
			end
			love.graphics.draw(self.ps, 0, 0) --> Position is set via ps:setPosition(), not during the draw call!
            if state.selection.alted or self.selected then
                self:drawHP()
            end
            if self.selected then
                self:drawSelectionCircle()
            end
		end

		function ret:apply(psEffectName, parameters) --> Applies a particle system state/animation to a unit render!
			particleStates:apply(self, psEffectName, parameters)
		end

		function ret:drawSelectionCircle()
			love.graphics.setColor(0, 255, 255, 255)
			love.graphics.setLineJoin("bevel")
			love.graphics.setLineWidth(2)
			love.graphics.setLineStyle("smooth")
			local h = (sim.hexagonWidth/2) * 0.75
			love.graphics.line(self.unit.x + (h-5) * math.cos(math.rad(0)), self.unit.y + (h-5) * math.sin(math.rad(0)), self.unit.x + (h+5) * math.cos(math.rad(0)), self.unit.y + (h+5) * math.sin(math.rad(0)))
			love.graphics.line(self.unit.x + (h-5) * math.cos(math.rad(60)), self.unit.y + (h-5) * math.sin(math.rad(60)), self.unit.x + (h+5) * math.cos(math.rad(60)), self.unit.y + (h+5) * math.sin(math.rad(60)))
			love.graphics.line(self.unit.x + (h-5) * math.cos(math.rad(120)), self.unit.y + (h-5) * math.sin(math.rad(120)), self.unit.x + (h+5) * math.cos(math.rad(120)), self.unit.y + (h+5) * math.sin(math.rad(120)))
			love.graphics.line(self.unit.x + (h-5) * math.cos(math.rad(180)), self.unit.y + (h-5) * math.sin(math.rad(180)), self.unit.x + (h+5) * math.cos(math.rad(180)), self.unit.y + (h+5) * math.sin(math.rad(180)))
			love.graphics.line(self.unit.x + (h-5) * math.cos(math.rad(240)), self.unit.y + (h-5) * math.sin(math.rad(240)), self.unit.x + (h+5) * math.cos(math.rad(240)), self.unit.y + (h+5) * math.sin(math.rad(240)))
			love.graphics.line(self.unit.x + (h-5) * math.cos(math.rad(300)), self.unit.y + (h-5) * math.sin(math.rad(300)), self.unit.x + (h+5) * math.cos(math.rad(300)), self.unit.y + (h+5) * math.sin(math.rad(300)))
		end

		function ret:drawHP()
			love.graphics.setColor(150, 0, 0, 255)
			love.graphics.setLineJoin("bevel")
			love.graphics.setLineWidth(4)
			love.graphics.setLineStyle("smooth")

			local function myStencilFunction()
			   	love.graphics.arc("fill", self.unit.x, self.unit.y, sim.hexagonWidth/2,  math.rad(-90) + math.rad(360 * self.unit.hp), math.rad(270), 100)
			end

			love.graphics.stencil(myStencilFunction, "replace", 1)
			love.graphics.setStencilTest("equal", 0)

			love.graphics.circle("line", self.unit.x, self.unit.y, (sim.hexagonWidth/2) * 0.75, 6)
			love.graphics.setStencilTest()
		end

		return ret

	end

}

return _unit
