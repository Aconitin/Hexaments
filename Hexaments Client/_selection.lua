local _selection = function()

	local _selection = {}
	
--[[
	vvvvv Variables!
--]]
	
	--> Some configuration!
	_selection.lineJoin = "bevel"
	_selection.lineStyle = "smooth"
	_selection.lineWidth = 1
	_selection.color = {0, 255, 255, 255}
	_selection.unitRectangle = 25 --> How large the selection rectangle for a unit is supposed to be!

	--> For info displaying!
	_selection.alted = false --> Basically, if alt is pressed, display HP of all units!
	
	
--[[
	^^^^^ Variables!
	vvvvv Basic functions!
--]]
	
	
	--> Update!
	function _selection:update(dt)
		if sim.map then
			local x, y = _map.getHexagonIndexByCoords(love.mouse.getX(), love.mouse.getY(), false)
			if x and y then
				state.selection.hovered = {x = x, y = y}
			else
				state.selection.hovered = nil
			end
		end
		state.selection.alted = love.keyboard.isDown("ralt", "lalt")
	end	
	
	--> Draw rectangle line!
	function _selection:draw()
		if state.selection.x and state.selection.w then
			love.graphics.setColor(self.color)
			love.graphics.setLineJoin(self.lineJoin)
			love.graphics.setLineStyle(self.lineStyle)
			love.graphics.setLineWidth(self.lineWidth)
			love.graphics.rectangle("line", state.selection.x, state.selection.y, state.selection.w, state.selection.h)
		end
	end

	
--[[
	^^^^^ Basic functions!
	vvvvv Callbacks & Events!
--]]
	
	
	--> Mouse pressed event!
	function _selection:mousepressed(x, y, button, istouch)
		if button == 1 then
			self:deselect()
			state.selection.x = x
			state.selection.y = y
			self:select(state.selection.x, state.selection.y)
		end
	end
	
	--> Mouse released event!
	function _selection:mousereleased(x, y, button, istouch)
		if button == 1 then
			self:select(state.selection.x, state.selection.y, state.selection.w, state.selection.h)
			state.selection.x = nil
			state.selection.y = nil
			state.selection.w = nil
			state.selection.h = nil
		end
	end

	--> Mouse moved event!
	function _selection:mousemoved(x, y, dx, dy)
		if love.mouse.isDown(1) and state.selection.x then --> Used for left-click drag selecting!
			state.selection.w = x - state.selection.x
			state.selection.h = y - state.selection.y
		end
	end

	
--[[
	^^^^^ Callbacks & Events!
	vvvvv Actual selection stuff!
--]]
	
	
	--> Select all units in rectangle!
	function _selection:select(x, y, w, h)
		local w, h = w or 1, h or 1
		local x1, y1 = state.camera:toWorld(x, y)
		local x2, y2 = state.camera:toWorld(x + w, y + h)
		local x1, x2, y1, y2 = math.min(x1, x2), math.max(x1, x2), math.min(y1, y2), math.max(y1, y2)
		for i,v in ipairs(sim.units) do --> Requires sim to be global and sim.units to be a thing!
			if v.x + self.unitRectangle >= x1 and v.x - self.unitRectangle <= x2 and v.y + self.unitRectangle >= y1 and v.y - self.unitRectangle <= y2 then --> Unit is to be selected!
				if v.owner.id == _nw.id and not v.dead then --> If the owner of the unit is this client, and the unit is alive, then you can select it!
					state.render.units[i].selected = true --> Beautiful. This works because state.render.units is a mirror of sim.units!
					if w == 1 and h == 1 then
						break
					end
				end
			end
		end
	end
	
	--> Deselect all units!
	function _selection:deselect()
		for i,v in ipairs(state.render.units) do
			v.selected = false
		end
	end
	
	
--[[
	^^^^^ Actual selection stuff!
	vvvvv Setters & Getters!
--]]
	
	
	--> Get currently hovered hexagon x/y or nil!
	function _selection:getHovered()
		return state.selection.hovered
	end
	
	--> Get currently selected unit indexes as a list!
	function _selection:getSelected()
		local ret = {}
		for i,v in ipairs(state.render.units) do
			if v.selected then
				table.insert(ret, i)
			end
		end
		return ret
	end
	
	--> Count currently selected units!
	function _selection:countSelected()
		local ret = 0
		for i,v in ipairs(state.render.units) do
			if v.selected then
				ret = ret + 1
			end
		end
		return ret
	end
		
	
--[[
	^^^^^ Setters & Getters!
--]]
	
	return _selection
	
end

return _selection()