local _nw = function()
 
    local self = {}
 
    self.host = nil --> Host to connect to!
    self.peer = nil --> Peer object returned after connection!
    self.id = nil --> Client ID on the server!
 
    self.out = {} --> Holds all outgoing updates (incoming are pushed into sim directly)!
 
    --> Update!
    function self:update(dt)
        while true do
            local event
            if self.host then
                event = self.host:service()
            end
            if event then
                if event.type == "connect" then --> When this client builds up a working connection with a server!
                    self.peer:send(_serial.serialize({type = "myname", content = configuration.name}))
                    sim = _sim() --> Global!
                    state = _gamestate() --> Global!
                elseif event.type == "disconnect" then --> When Server times out or _nw.disconnect() is invoked!
                    self:disconnect()
                elseif event.type == "receive" then
                    local data = _serial.deserialized(event.data)
                    if data.type == "yourId" then
                        self.id = data.content
                    else
                        sim:push(data) --> Push all incoming data onto the state.inc update stack!
                    end
                end
            else
                break
            end
        end
        --> Send all outgoing updates!
        while self.peer and self.host and #self.out ~= 0 do
            local data = table.remove(_nw.out, 1)
            if not data then
                break
            end
            _nw.peer:send(_serial.serialize(data))
        end
    end
 
    --> Kill all the connection-relevant objects!
    function self:disconnect()
        if _nw.peer then
            _nw.peer:disconnect()
            _nw.host:flush()
        end
        _nw.host = nil
        _nw.peer = nil
        _nw.id = nil
        _nw.out = {}
        sim = nil
        state = nil
        collectgarbage() --> Just to be safe!
		return love.load()
    end
 
    --> Networking mit diesem PC als Client, peer ist IP:Port des Servers an den verbunden wird!
    function self:connect(peer)
        _nw.host = enet.host_create()
        local success = false
        success, _nw.peer = pcall(_nw.host.connect, _nw.host, peer)
        if success == false then
            return "Could not create a host at " .. peer, false
        else
            return "Successfully created a host at " .. peer, true
        end
    end
 
    --> Returns status of networking!
    function self:getStatus()
        if _nw.peer then
            return _nw.peer:state()
        else
            return "Offline"
        end
    end
 
    function self:getHost()
        return self.host
    end
 
    --> Pushes outgoing update to stack!
    function self:push(data)
        table.insert(_nw.out, data)
    end
 
    return self
 
end
   
return _nw()