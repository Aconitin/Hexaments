local _debug = function()
	
	local _debug = {}
 
	_debug.visible = true
	 
	function _debug:draw()
		if self.visible then
			love.graphics.print("FPS: " .. love.timer.getFPS() .. "\n"
								.. "\nKeys:\n-----\n"
								.. "ctrl + alt + n + enter: Toggle debug menu\n"
								.. "r: Reload game codebase on-the-fly\n"
								, 10, 10)
		end
	end
	 
	function _debug:toggle(visibility)
		if visibility then
			self.visible = visibility
		else
			self.visible = not self.visible
		end
		if self.visible then
			love.window.setTitle("Hexaments | Debug Menu enabled")
		else
			love.window.setTitle("Hexaments")
		end
	end
	
	function _debug:keypressed(key, isrepeat)
		if self.visible then
			if key == "r" then
				love.load("debugReset")
			end
		end
	end
	
	return _debug
	
end
 
return _debug()