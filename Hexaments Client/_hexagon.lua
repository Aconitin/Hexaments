local _hexagon = {
	
	--> Draws a white hexagon (for fog of war) at specified hexagon location!
	drawWhitemapFilling = function(i_hexagon, ox, oy, color)
		local ox = ox or 0
		local oy = oy or 0
		local color = color or {255, 255, 255, 255}
		love.graphics.setColor(color)
		love.graphics.circle("fill", i_hexagon.x + ox, i_hexagon.y + oy, sim.hexagonWidth/2, 6)
	end,
	
	--> Draws the border lines of a given hexagon!
	drawBorder = function(i_hexagon, color, width, style, join)
		local color = color or {0, 255, 255, 255} --> Cyan!
		local width = width or 2
		local style = style or "smooth"
		local join = join or "bevel"
		love.graphics.setLineWidth(width)
		love.graphics.setLineStyle(style)
		love.graphics.setLineJoin(join)
		love.graphics.setColor(color)
		love.graphics.line(i_hexagon.borderPointsWithHeight) --> Draws the hovered hexagon!
		
		--> Draw additional lines indicating height!
		if i_hexagon.biome == "mountains" then
			for i,v in pairs({1, 4, 5, 6}) do --> Only draw 4 out of 6 lines!
				local v = i_hexagon.points[v]
				love.graphics.line(v.x, v.y, v.x, v.y - i_hexagon.height)
			end
		end
		
	end,
	
	--> Returns true if given point is inside specified hexagon, false otherwise!
	isPointInside = function(hexagonCenterX, hexagonCenterY, hexagonWidth, pointX, pointY)
		local px = math.abs(pointX - hexagonCenterX)
		local py = math.abs(pointY - hexagonCenterY)
		local v = 0.25 * hexagonWidth
		local h = 2 * v * math.cos(math.rad(30))
		if (py > h) or (px > 2 * v) then
			return false
		end
		return 2*v * h - v*py - h * px >= 0
	end,
	
	--> Return all neighbours of a given hexagon coordinate!
	getNeighbours = function(x, y, withSelf)
		local neighbours
		if x % 2 == 1 then
			neighbours = {
					{x = x, y = y-1},
					{x = x, y = y+1},
					{x = x-1, y = y-1},
					{x = x-1, y = y},
					{x = x+1, y = y-1},
					{x = x+1, y = y},
			}
		else
			neighbours = {
					{x = x, y = y-1},
					{x = x, y = y+1},
					{x = x-1, y = y},
					{x = x-1, y = y+1},
					{x = x+1, y = y},
					{x = x+1, y = y+1},
			}
		end
		if withSelf then --> Include the middle tile, too, not only the neighbours!
			table.insert(neighbours, 1, {x = x, y = y})
		end
		for i=#neighbours,1,-1 do
			local v = neighbours[i]
			if v.x < sim.lowestColumnIndex or v.y < sim.lowestRowIndex or v.x > sim.highestColumnIndex or v.y > sim.highestRowIndex then
				table.remove(neighbours, i)
			end
		end
		return neighbours
	end,
	
	--> Checks if for a given hexagon index x/y there is renderInfo available. If yes, returns that info. If not, sets it to specified value. Also returns indicator if info was overwritten or not!
	getOrSetRenderInfo = function(x, y, field, contents)
		if not state.render.hexRenderInfo[x] then
			state.render.hexRenderInfo[x] = {}
		end
		if not state.render.hexRenderInfo[x][y] then
			state.render.hexRenderInfo[x][y] = {}
		end
		local renderInfo = state.render.hexRenderInfo[x][y][field]
		if renderInfo then
			return renderInfo, true
		end
		state.render.hexRenderInfo[x][y][field] = contents
		return contents, false
	end,

} return _hexagon