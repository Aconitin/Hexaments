local _map = { --> Class that generates and renders the map!

	--> Draws the entire map!
	draw = function()

		--> First: Clear everything. Every spritebatch, every animation!
		state.render.grassSpriteBatch:clear()
		state.render.beachSpriteBatch:clear()
		state.render.waterSpriteBatch:clear()
		for i=1,#state.render.dotCanvas,1 do
			state.render.dotSpriteBatch[i]:clear()
		end
		state.render.anim8.animationCoordinates.water = {}
		state.render.visibleHexagons = {}
			
		--> All data neccessary to draw the map is more or less generated on-the-fly. The next part here does exactly this!
		--> This fetches all hexagons that are on-screen (plus some) and have to be drawn. Returns a table of tables, as in sim.map.hexagons!
		state.render.visibleHexagons = _map.getHexagonsInRectangle(-2 * sim.hexagonWidth, -2 * sim.hexagonHeight, love.graphics.getWidth() + 2 * sim.hexagonWidth, love.graphics.getHeight() + 2 * sim.hexagonHeight)
		for hx, _ in pairs(state.render.visibleHexagons) do
			for hy, v in pairs(_) do

				--> Now have a look at the terrain type!
				if v.biome == "grass" then
					local py = (1 - v.elevation) * (state.render.images.grassLookup:getHeight() - 1)
					local r, g, b, a = state.render.images.grassLookup:getPixel(0, py) --> Grass color is determined by image lookup (green-ness)!
					_map.addToSpritebatch(state.render.grassSpriteBatch, {r, g, b, a}, v.x, v.y, 0, 1, 1, 0.5 * state.render.hexagonCanvas:getWidth(), 0.5 * state.render.hexagonCanvas:getHeight())
					local renderInfo = _hexagon.getOrSetRenderInfo(hx, hy, "dotInfo", {index = love.math.random(1,10), angle = love.math.random(1,6)*60})
					_map.addToSpritebatch(state.render.dotSpriteBatch[renderInfo.index], nil, v.x, v.y, math.rad(renderInfo.angle), 1, 1, 0.5 * sim.hexagonWidth, 0.5 * sim.hexagonHeight)
	
				elseif v.biome == "beach" then
					local r, g, b, a = 199, 176, 145, 255 --> Beach color!
					_map.addToSpritebatch(state.render.beachSpriteBatch, {r, g, b, a}, v.x, v.y, 0, 1, 1, 0.5 * state.render.hexagonCanvas:getWidth(), 0.5 * state.render.hexagonCanvas:getHeight())
					local renderInfo = _hexagon.getOrSetRenderInfo(hx, hy, "dotInfo", {index = love.math.random(1,10), angle = love.math.random(1,6)*60})
					_map.addToSpritebatch(state.render.dotSpriteBatch[renderInfo.index], nil, v.x, v.y, math.rad(renderInfo.angle), 1, 1, 0.5 * sim.hexagonWidth, 0.5 * sim.hexagonHeight)

				elseif v.biome == "water" then
					local r, g, b, a = 12, 50, 120, 255
					_map.addToSpritebatch(state.render.waterSpriteBatch, {r, g, b, a}, v.x, v.y, 0, 1, 1, 0.5 * state.render.hexagonCanvas:getWidth(), 0.5 * state.render.hexagonCanvas:getHeight())
					table.insert(state.render.anim8.animationCoordinates.water, {x = v.x, y = v.y})
					
				elseif v.biome == "mountains" then
					local renderInfo = _hexagon.getOrSetRenderInfo(hx, hy, "dotInfo", {index = love.math.random(1,10), angle = love.math.random(1,6)*60})
					_map.addToSpritebatch(state.render.dotSpriteBatch[renderInfo.index], nil, v.x, v.y, math.rad(renderInfo.angle), 1, 1, 0.5 * sim.hexagonWidth, 0.5 * sim.hexagonHeight)
					_map.addToSpritebatch(state.render.dotSpriteBatch[renderInfo.index], nil, v.x, v.y - v.height, math.rad(renderInfo.angle + 180), 1, 1, 0.5 * sim.hexagonWidth, 0.5 * sim.hexagonHeight)
					local _, addedToMesh = _hexagon.getOrSetRenderInfo(hx, hy, "addedToMesh", true)
					if not addedToMesh then --> Add to mesh. Duh!
						--> Set mountain colors!
						local pointColor = {}
						pointColor.top = {r = 255*v.elevation, g = 255*v.elevation, b = 255*v.elevation, a = 255} --> Color of mountain top!
						pointColor.middle = {r = 130, g = 130, b = 130, a = 255} --> Color of mountain sides!
						pointColor.sides = {r = 100, g = 100, b = 100, a = 255} --> Color of mountain front!
						--> Quickly fetch all mountain mesh points. This is ugly, but fast!
						local pointCoordinates = {y = v.y, points = {
							{v.points[1].x, v.points[1].y - v.height, pointColor.sides},
							{v.points[6].x, v.points[6].y - v.height, pointColor.sides},
							{v.points[1].x, v.points[1].y, pointColor.sides},
							{v.points[6].x, v.points[6].y - v.height, pointColor.sides},
							{v.points[1].x, v.points[1].y, pointColor.sides},
							{v.points[6].x, v.points[6].y, pointColor.sides},	
							{v.points[6].x, v.points[6].y - v.height, pointColor.middle},
							{v.points[5].x, v.points[5].y, pointColor.middle},
							{v.points[6].x, v.points[6].y, pointColor.middle},
							{v.points[6].x, v.points[6].y - v.height, pointColor.middle},
							{v.points[5].x, v.points[5].y - v.height, pointColor.middle},
							{v.points[5].x, v.points[5].y, pointColor.middle},
							{v.points[4].x, v.points[4].y, pointColor.sides},
							{v.points[5].x, v.points[5].y - v.height, pointColor.sides},
							{v.points[5].x, v.points[5].y, pointColor.sides},
							{v.points[4].x, v.points[4].y, pointColor.sides},
							{v.points[5].x, v.points[5].y - v.height, pointColor.sides},
							{v.points[4].x, v.points[4].y - v.height, pointColor.sides},
							{v.points[1].x, v.points[1].y - v.height, pointColor.top},
							{v.points[2].x, v.points[2].y - v.height, pointColor.top},
							{v.x, v.y - v.height, pointColor.top},
							{v.points[2].x, v.points[2].y - v.height, pointColor.top},
							{v.points[3].x, v.points[3].y - v.height, pointColor.top},
							{v.x, v.y - v.height, pointColor.top},
							{v.points[4].x, v.points[4].y - v.height, pointColor.top},
							{v.points[3].x, v.points[3].y - v.height, pointColor.top},
							{v.x, v.y - v.height, pointColor.top},
							{v.points[4].x, v.points[4].y - v.height, pointColor.top},
							{v.points[5].x, v.points[5].y - v.height, pointColor.top},
							{v.x, v.y - v.height, pointColor.top},
							{v.points[6].x, v.points[6].y - v.height, pointColor.top},
							{v.points[5].x, v.points[5].y - v.height, pointColor.top},
							{v.x, v.y - v.height, pointColor.top},
							{v.points[1].x, v.points[1].y - v.height, pointColor.top},
							{v.points[6].x, v.points[6].y - v.height, pointColor.top},
							{v.x, v.y - v.height, pointColor.top}
						}}
						table.insert(state.render.meshPoints, pointCoordinates)
						local allPoints = {}
						table.sort(state.render.meshPoints, function(a, b) return a.y < b.y end)
						for _,tbl in ipairs(state.render.meshPoints) do
							for _, pts in ipairs(tbl.points) do
								table.insert(allPoints, {pts[1], pts[2], 0, 0, pts[3].r, pts[3].g, pts[3].b, pts[3].a})
							end
						end
						state.render.mountainMesh = love.graphics.newMesh(allPoints, "triangles", "dynamic") --> This happens everytime a new mountain is discovered. Caution: Doesn't scale well with a lot of mountains!
					end
				end
			end
		end

		--> Now draw it all. Terrain first!
		love.graphics.setColor(255, 255, 255, 255)
		love.graphics.setBlendMode("alpha", "premultiplied")
		love.graphics.draw(state.render.waterSpriteBatch) --> Draw plain water!
		love.graphics.setBlendMode("alpha", "alphamultiply")
		love.graphics.setColor(55, 122, 246, 255)
		for i,v in pairs(state.render.anim8.animationCoordinates.water) do
			state.render.anim8.animations.waterAnimation:draw(state.render.images.waves, v.x, v.y, 0, 1, 1, 0.5 * sim.hexagonWidth, 0.5 * sim.hexagonHeight) --> Now draw a waves animation on top of all water tiles!
		end
		love.graphics.setColor(255, 255, 255, 255)
		love.graphics.setBlendMode("alpha", "premultiplied")
		love.graphics.draw(state.render.beachSpriteBatch) --> Draw all beaches!
		love.graphics.draw(state.render.grassSpriteBatch) --> Draw grass last!
		love.graphics.setBlendMode("alpha", "alphamultiply")
		love.graphics.setColor(255, 255, 255, 255)
		if state.render.mountainMesh then --> Only do that if there are mountains!
			love.graphics.draw(state.render.mountainMesh) --> Now draw all mountains on top of that. Order is super important because otherwise the mountains will overlap very weirdly!
		end	
		love.graphics.setBlendMode("alpha", "premultiplied")
		for i=1,#state.render.dotCanvas,1 do
			love.graphics.draw(state.render.dotSpriteBatch[i]) --> Then draw the decoDots on top of everything!
		end
		love.graphics.setBlendMode("alpha", "alphamultiply")

	end,
	
	--> Returns a subset of sim.map.hexagons containing only those which are visible on-screen at function call!
	getHexagonsInRectangle = function(x, y, w, h)
	
		local wx, wy = state.camera:toWorld(x, y) --> get world coords out of screen coords!
		local minX, minY = math.ceil(wx / ((0.5 * sim.hexagonWidth) * 1.5)), math.ceil(wy / ((0.5 * sim.hexagonHeight) * 2)) --> Gets approximate column/row in which the point would be!
		local maxX, maxY = math.ceil((wx + w) / ((0.5 * sim.hexagonWidth) * 1.5)), math.ceil((wy + h) / ((0.5 * sim.hexagonHeight) * 2)) --> Gets approximate column/row in which the point would be!

		--> Clamp some values and account for special cases!
		if minX < sim.lowestColumnIndex then
			minX = sim.lowestColumnIndex
		end
		if minY < sim.lowestRowIndex then
			minY = sim.lowestRowIndex
		end
		if maxX > sim.highestColumnIndex then
			maxX = sim.highestColumnIndex
		end
		if maxY > sim.highestRowIndex then
			maxY = sim.highestRowIndex
		end
		
		local hexagons = {} --> Hexagon list to return!
		
		if minX <= sim.highestColumnIndex and maxX >= sim.lowestColumnIndex and minY <= sim.highestRowIndex and maxY >= sim.lowestRowIndex then --> Only proceed when the rectangle is over the map!
			for ix=minX,maxX,1 do
				for iy=minY,maxY,1 do
					if sim.map.hexagons[ix] then
						if sim.map.hexagons[ix][iy] then
							if not hexagons[ix] then
								hexagons[ix] = {}
							end
							hexagons[ix][iy] = sim.map.hexagons[ix][iy]
						end
					end
				end
			end	
		end
		return hexagons
	end,
	
	--> Returns the hexagon index (x/y) for the hexagon that lies below the on-screen coords x/y (or nil)!
	getHexagonIndexByCoords = function(coordX, coordY, plain) --> If plain is true then it does not care about mountains (and other 3d-ish tiles)!
	
		--> Get an approximate list of possible candidates!
		local hexagonWidth = 0.5 * sim.hexagonWidth
		local hexagonHeight = 0.5 * sim.hexagonHeight
		local mapCornerX, mapCornerY = state.camera:toScreen(0, 0) --> Dirty
		local px, py = coordX - mapCornerX, coordY - mapCornerY --> Point to test against the hexagons!
		
		--> Make an approximation!
		local rectangleX = math.ceil(px / (hexagonWidth * 1.5))
		local rectangleY = math.ceil(py / (hexagonHeight * 2))
		local maybe = _hexagon.getNeighbours(rectangleX, rectangleY, true) --> List of possible candidates!

		if not plain then
			--> Mountains always have priority in hover-selection!
			local mountains = {}
			for i=#maybe,1,-1 do
				local v = maybe[i]
				if sim.map.hexagons[v.x][v.y].biome == "mountains" then
					table.insert(mountains, v) --> Fetch all mountains out of the maybe-table and process them first!
					table.remove(maybe, i)
				end
			end
			
			--> If there are mountains involved, process them!
			for i=#mountains,1,-1 do
				local v = mountains[i]
				local hex = sim.map.hexagons[v.x][v.y]
				local c1x, c1y = state.camera:toScreen(hex.x, hex.y - hex.height)
				local c2x, c2y = state.camera:toScreen(hex.x, hex.y)
				local p1x, p1y = state.camera:toScreen(hex.points[1].x, hex.points[1].y)
				local p2x, p2y = state.camera:toScreen(hex.points[4].x, hex.points[4].y - hex.height)
				if not (_hexagon.isPointInside(c1x, c1y, sim.hexagonWidth, coordX, coordY)
						or _hexagon.isPointInside(c2x, c2y, sim.hexagonWidth, coordX, coordY)
						or (coordX >= p1x and coordY <= p1y and coordX <= p2x and coordY >= p2y)) then
					table.remove(mountains, i)
				end
				
			end
		
			--> If there are any mountains left where the mouse is actually over, then proceed!
			if #mountains ~= 0 then
				table.sort(mountains, function(a,b) return sim.map.hexagons[a.x][a.y].y > sim.map.hexagons[b.x][b.y].y end) --> Mountains that are lower on the map have higher proirity in getting hovered over!
				return mountains[1].x, mountains[1].y
			end
	
		end
	
		--> Now process all the other, flat tiles!
		for i,v in ipairs(maybe) do
			local cx, cy = state.camera:toScreen(sim.map.hexagons[v.x][v.y].x, sim.map.hexagons[v.x][v.y].y)
			if _hexagon.isPointInside(cx, cy, sim.hexagonWidth, coordX, coordY) then
				return v.x, v.y
			end
		end

	end,

	--> Last thing to draw (fog of war)!
	drawShading = function()
		--> Build shaders!
		local x, y = state.camera:getPosition()
		local ox = ((love.graphics.getWidth()/2)-sim.mapWidth/2)+(sim.mapWidth/2-x)
		local oy = ((love.graphics.getHeight()/2)-sim.mapHeight/2)+(sim.mapHeight/2-y)
		_map.drawWhitemap(state.render.whitemap, ox, oy) --> Draws whitemap to canvas!
		state.render.whitemap = _effect.map["fovBlur"]:draw(state.render.whitemap) --> Blurs whitemap!
		local nebula = _effect.map["nebula"]:draw(state.render.whitemap) --> Fetches nebula shading!
		--> Draws everything on top!
		love.graphics.setBlendMode("darken", "premultiplied")
		love.graphics.draw(state.render.whitemap)
		love.graphics.setBlendMode("lighten", "premultiplied")
		love.graphics.draw(nebula)
		love.graphics.setBlendMode("alpha", "alphamultiply")
	end,

	--> Draws a white version of the map onto a given canvas!
	drawWhitemap = function(canvas, ox, oy)
		local i_map = sim.map
		love.graphics.setCanvas(canvas)
		love.graphics.setColor(0, 0, 0, 255)
		love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
		love.graphics.setBlendMode("replace")
		love.graphics.setColor(255, 255, 255, 255)
		for wi,v in pairs(state.render.visibleHexagons) do
			if wi ~= sim.highestColumnIndex and wi ~= sim.lowestColumnIndex then
				for hi, i_hexagon in pairs(v) do
					if hi ~= sim.highestRowIndex and hi ~= sim.lowestRowIndex then
						_hexagon.drawWhitemapFilling(i_hexagon, ox, oy)
					end
				end
			end
		end
		love.graphics.setBlendMode("alpha")
		love.graphics.setCanvas()
	end,
		
	--> Adds to a spriteBatch and keeps sizes in mind!
	addToSpritebatch = function(spriteBatch, color, x, y, r, sx, sy, ox, oy)
		local color = color or {255, 255, 255, 255}
		if spriteBatch:getCount() == spriteBatch:getBufferSize() then
			spriteBatch:setBufferSize(spriteBatch:getBufferSize() + 1)
		end
		spriteBatch:setColor(color)
		spriteBatch:add(x, y, r, sx, sy, ox, oy)
	end,

} return _map

