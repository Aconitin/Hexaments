--[[
	This effect creates a cool nebula which can be used for clouds, water, fog, etc. However, it is quite resource-heavy (especially in fullscreen mode), so handle with care!
--]]

local function _nebula(c_effect, name, description, autoUpdate, autoDraw, drawIndex, parameters) --> Returns a new nebula object!
	local ret = {}
	ret.name = name or "NoName" --> Identifier for the map!
	ret.description = description or "No description set!"
	ret.autoUpdate = autoUpdate or false --> Determines wether the nebula will be updated by _effect.updateAll() or not!
	ret.autoDraw = autoDraw or false --> Toggle drawing with all the other effects!
	ret.drawIndex = drawIndex or 0 --> Place in drawing order!
	-->
	parameters = parameters or {}
	ret.samples = parameters.samples or 25 --> The more, the more blur and the slower it is!
	ret.downScaleFactor = parameters.downScaleFactor or 8 --> The more, the more it blurs and the faster it is!
	ret.w = love.graphics.getWidth()/ret.downScaleFactor
	ret.h = love.graphics.getHeight()/ret.downScaleFactor
	ret.scaledCanvas1 = love.graphics.newCanvas(ret.w, ret.h) --> For Canvas-Ping-Pong!
	ret.scaledCanvas2 = love.graphics.newCanvas(ret.w, ret.h) --> For Canvas-Ping-Pong!
	-->
	ret.shaderVertical = love.graphics.newShader [[
		extern float Height;
		const vec2 offset_1 = vec2(0.0, 1.3846153846);
		const vec2 offset_2 = vec2(0.0, 3.2307692308);
		const vec3 weight = vec3(0.2270270270, 0.3162162162, 0.0702702703);
		vec4 effect(vec4 color, Image texture, vec2 texcoords, vec2 pixcoords)
		{
			vec2 offset1 = offset_1 / Height;
			vec2 offset2 = offset_2 / Height;
			vec3 tc = Texel(texture, texcoords).rgb * weight[0];
			tc += Texel(texture, texcoords + offset1).rgb * weight[1];
			tc += Texel(texture, texcoords - offset1).rgb * weight[1];
			tc += Texel(texture, texcoords + offset2).rgb * weight[2];
			tc += Texel(texture, texcoords - offset2).rgb * weight[2];
			return vec4(tc, 1.0);
		}
	]]
	-->
	ret.shaderHorizontal = love.graphics.newShader [[
		extern float Width;
		const vec2 offset_1 = vec2(1.3846153846, 0.0);
		const vec2 offset_2 = vec2(3.2307692308, 0.0);
		const vec3 weight = vec3(0.2270270270, 0.3162162162, 0.0702702703);
		vec4 effect(vec4 color, Image texture, vec2 texcoords, vec2 pixcoords)
		{
			vec2 offset1 = offset_1 / Width;
			vec2 offset2 = offset_2 / Width;
			vec3 tc = Texel(texture, texcoords).rgb * weight[0];
			tc += Texel(texture, texcoords + offset1).rgb * weight[1];
			tc += Texel(texture, texcoords - offset1).rgb * weight[1];
			tc += Texel(texture, texcoords + offset2).rgb * weight[2];
			tc += Texel(texture, texcoords - offset2).rgb * weight[2];
			return vec4(tc, 1.0);
		}
	]]
	-->
	ret.shaderPingPong = function(shader, canvasFrom, canvasTo)
		love.graphics.setColor(255, 255, 255, 255)
		love.graphics.setShader(shader)
		canvasTo:renderTo(function()
			love.graphics.draw(canvasFrom, 0, 0, 0)
		end)
		love.graphics.setShader()
		love.graphics.setCanvas(canvasFrom)
		love.graphics.clear()
		love.graphics.setCanvas()
	end
	-->
	ret.draw = function(i_blur, canvas) --> Shaders are generally drawn from a given canvas to another given canvas!

		love.graphics.setCanvas(i_blur.scaledCanvas1)
		love.graphics.clear()
		love.graphics.setCanvas()
		love.graphics.setCanvas(i_blur.scaledCanvas2)
		love.graphics.clear()
		love.graphics.setCanvas()
		
		love.graphics.setColor(255, 255, 255, 255)
		i_blur.scaledCanvas1:renderTo(function()
			love.graphics.draw(canvas, 0, 0, 0, 1/i_blur.downScaleFactor, 1/i_blur.downScaleFactor)
		end)
		love.graphics.setCanvas(canvas)
		love.graphics.clear()
		love.graphics.setCanvas()
		for i=1, i_blur.samples, 1 do
			i_blur.shaderPingPong(i_blur.shaderVertical, i_blur.scaledCanvas1, i_blur.scaledCanvas2)
			i_blur.shaderPingPong(i_blur.shaderHorizontal, i_blur.scaledCanvas2, i_blur.scaledCanvas1)
		end
		love.graphics.setColor(255, 255, 255, 255)
		canvas:renderTo(function()
			love.graphics.draw(i_blur.scaledCanvas1, 0, 0, 0, i_blur.downScaleFactor, i_blur.downScaleFactor)
		end)
		love.graphics.setCanvas(i_blur.scaledCanvas1)
		love.graphics.clear()
		love.graphics.setCanvas()
		love.graphics.setColor(255, 255, 255, 255)
		return canvas
	end
	-->
	ret.shaderVertical:send("Height", ret.h)
	ret.shaderHorizontal:send("Width", ret.w)
	table.insert(c_effect.instances, ret)
	c_effect.map[ret.name] = ret
end

return _nebula