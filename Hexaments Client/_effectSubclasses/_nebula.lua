--[[
	This effect creates a cool nebula which can be used for clouds, water, fog, etc. However, it is quite resource-heavy (especially in fullscreen mode), so handle with care!
--]]

local function _nebula(c_effect, name, description, autoUpdate, autoDraw, drawIndex, parameters) --> Returns a new nebula object!
	local ret = {}
	ret.name = name or "NoName" --> Identifier for the map!
	ret.description = description or "No description set!"
	ret.autoUpdate = autoUpdate or false --> Determines wether the nebula will be updated by _effect.updateAll() or not!
	ret.autoDraw = autoDraw or false --> Toggle drawing with all the other effects!
	ret.drawIndex = drawIndex or 0 --> Place in drawing order!
	-->
	parameters = parameters or {}
	love.math.setRandomSeed(os.time())
	ret.time = parameters.t or love.math.random(1, 100) --> The current effect time!
	ret.timeMultiplier = parameters.tm or love.math.random(25, 35)/1000 --> A multiplier for the time. E.g. a multiplier of 2 makes the nebula moves twice as fast!
	ret.scale = parameters.scale or love.math.random(150, 250) --> Size of clouds of nebula!
	ret.rc = parameters.rc or love.math.random(1, 10)/10 --> Red multiplier!
	ret.gc = parameters.gc or love.math.random(1, 10)/10 --> Green multiplier!
	ret.bc = parameters.bc or love.math.random(1, 10)/10 --> Blue multiplier!
	ret.a = parameters.a or love.math.random(55, 100)/100 --> Alpha value!
	-->
	ret.fadeTo = {ret.rc, ret.gc, ret.bc, ret.a}
	ret.fadeTime = 0
	ret.fadeFactors = {1, 1, 1, 1}
	ret.canvas = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight(), "normal", 0)
	-->
	ret.shader = love.graphics.newShader [[
		vec3 mod289(vec3 x){
			return x - floor(x * (1.0 / 289.0)) * 289.0;
		}
		vec4 mod289(vec4 x){
			return x - floor(x * (1.0 / 289.0)) * 289.0;
		}
		vec4 permute(vec4 x){
			return mod289(((x*34.0)+1.0)*x);
		}
		vec4 taylorInvSqrt(vec4 r){
			return 1.79284291400159 - 0.85373472095314 * r;
		}
		float snoise(vec3 v){ 
			const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;
			const vec4  D = vec4(0.0, 0.5, 1.0, 2.0);
			vec3 i  = floor(v + dot(v, C.yyy));
			vec3 x0 =   v - i + dot(i, C.xxx);
			vec3 g = step(x0.yzx, x0.xyz);
			vec3 l = 1.0 - g;
			vec3 i1 = min(g.xyz, l.zxy);
			vec3 i2 = max(g.xyz, l.zxy);
			vec3 x1 = x0 - i1 + C.xxx;
			vec3 x2 = x0 - i2 + C.yyy;
			vec3 x3 = x0 - D.yyy;
			i = mod289(i); 
			vec4 p = permute( permute( permute( 
				i.z + vec4(0.0, i1.z, i2.z, 1.0 ))
				+ i.y + vec4(0.0, i1.y, i2.y, 1.0 )) 
				+ i.x + vec4(0.0, i1.x, i2.x, 1.0 ));
			float n_ = 0.142857142857;
			vec3  ns = n_ * D.wyz - D.xzx;
			vec4 j = p - 49.0 * floor(p * ns.z * ns.z);
			vec4 x_ = floor(j * ns.z);
			vec4 y_ = floor(j - 7.0 * x_ );
			vec4 x = x_ *ns.x + ns.yyyy;
			vec4 y = y_ *ns.x + ns.yyyy;
			vec4 h = 1.0 - abs(x) - abs(y);
			vec4 b0 = vec4( x.xy, y.xy );
			vec4 b1 = vec4( x.zw, y.zw );
			vec4 s0 = floor(b0)*2.0 + 1.0;
			vec4 s1 = floor(b1)*2.0 + 1.0;
			vec4 sh = -step(h, vec4(0.0));
			vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
			vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;
			vec3 p0 = vec3(a0.xy,h.x);
			vec3 p1 = vec3(a0.zw,h.y);
			vec3 p2 = vec3(a1.xy,h.z);
			vec3 p3 = vec3(a1.zw,h.w);
			vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
			p0 *= norm.x;
			p1 *= norm.y;
			p2 *= norm.z;
			p3 *= norm.w;
			vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
			m = m * m;
			return 42.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1), dot(p2,x2), dot(p3,x3) ) );
		}
		extern float time;
		extern float scale;
		extern float rc;
		extern float gc;
		extern float bc;
		extern float a;
		
		vec4 effect(vec4 vcolor, Image texture, vec2 texcoord, vec2 pixcoord){   
			vec3 pos = vec3(pixcoord / scale, time);
			float sum = snoise(pos);
			sum += snoise(pos * 2.0) / 2.0;
			sum += snoise(pos * 4.0) / 4.0;
			sum += snoise(pos * 8.0) / 8.0;
			sum += snoise(pos * 16.0) / 16.0;
			sum = 0.25 * sum + 0.25;
			return vec4(sum*rc, sum*gc, sum*bc, a);
		}
	]] --> The shader. Thank you, Ashima Arts!
	-->
	ret.draw = function(i_nebula, wm) --> Shaders are generally drawn from a given canvas to another given canvas!

		love.graphics.setCanvas(i_nebula.canvas)
		love.graphics.clear(255, 255, 255, 0)
		love.graphics.setCanvas()

		love.graphics.setShader(i_nebula.shader)
		love.graphics.setColor(255, 255, 255, 255)
		i_nebula.canvas:renderTo(function () love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight()) end)
		love.graphics.setShader()

		return i_nebula.canvas

	end
	-->
	ret.update = function(i_nebula, dt) --> If not given a dt, animation will pause!
		if dt then
			i_nebula.time = i_nebula.time + dt*i_nebula.timeMultiplier
			i_nebula.shader:send("time", i_nebula.time)
		end
		if i_nebula.fadeTime < 0 then
			i_nebula.fadeTime = 0
		elseif i_nebula.fadeTime > 0 then
			i_nebula.fadeTime = i_nebula.fadeTime - dt
			i_nebula.rc = i_nebula.rc + i_nebula.fadeFactors.rc*dt
			i_nebula.gc = i_nebula.gc + i_nebula.fadeFactors.gc*dt
			i_nebula.bc = i_nebula.bc + i_nebula.fadeFactors.bc*dt
			i_nebula.a = i_nebula.a + i_nebula.fadeFactors.a*dt
			i_nebula.shader:send("rc", i_nebula.rc)
			i_nebula.shader:send("gc", i_nebula.gc)
			i_nebula.shader:send("bc", i_nebula.bc)
			i_nebula.shader:send("a", i_nebula.a)
		end
	end
	-->
	ret.setColor = function(i_nebula, rc, gc, bc, a, t) --> Sets color to given values. If t is given, fades colors!
		i_nebula.fadeTime = t or 0
		if i_nebula.fadeTime ~= 0 then
			i_nebula.fadeTo = {i_nebula.rc, i_nebula.gc, i_nebula.bc, i_nebula.a}
			i_nebula.fadeFactors = {
				rc = (rc-i_nebula.rc)/t,
				gc = (gc-i_nebula.gc)/t,
				bc = (bc-i_nebula.bc)/t,
				a = (a-i_nebula.a)/t
			}
		else
			i_nebula.rc = rc or i_nebula.rc
			i_nebula.gc = gc or i_nebula.gc
			i_nebula.bc = bc or i_nebula.bc
			i_nebula.a = a or i_nebula.a
			i_nebula.shader:send("rc", i_nebula.rc)
			i_nebula.shader:send("gc", i_nebula.gc)
			i_nebula.shader:send("bc", i_nebula.bc)
			i_nebula.shader:send("a", i_nebula.a)
		end
	end
	-->
	ret.getColor = function(i_nebula)
		return {i_nebula.rc, i_nebula.gc, i_nebula.bc, i_nebula.a}
	end
	-->
	ret.shader:send("time", ret.time)
	ret.shader:send("scale", ret.scale)
	ret.shader:send("rc", ret.rc)
	ret.shader:send("gc", ret.gc)
	ret.shader:send("bc", ret.bc)
	ret.shader:send("a", ret.a)
	table.insert(c_effect.instances, ret)
	c_effect.map[ret.name] = ret
end

return _nebula