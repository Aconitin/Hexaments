--[[
	This class provides a collection of very cool effects, shaders, animations, etc!
--]]

local _effect = function()
	
	local _effect = {
	
	_nebula = require("_effectSubclasses/_nebula"),
	_blur = require("_effectSubclasses/_blur"),
	
	instances = {},
	map = {}, --> Maps effect name to instances, e.g. map["backgroundNebula"]!
	canvas1 = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight()),
	canvas2 = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight()),
	
	updateAll = function(c_effect, dt) --> Updates all effects that have "autoUpdate" set to true!
		for i,v in pairs(c_effect.instances) do
			if v.autoUpdate and v.update then
				v:update(dt)
			end
		end
		c_effect:checkRemovals()
		c_effect:refreshMapping()
	end,
	
	refreshMapping = function(c_effect)
		c_effect.map = {}
		for i,v in ipairs(c_effect.instances) do
			c_effect.map[v.name] = v
		end
	end,
	
	checkRemovals = function(c_effect)
		for i=#c_effect.instances, 1, -1 do
			if c_effect.instances[i].removeable then
				table.remove(c_effect.instances, i)
			end
		end
	end,
	
	getAutoDrawEffects = function(c_effect)
		local ret = {}
		for i,v in ipairs(c_effect.instances) do
			if v.autoDraw and v.draw then
				table.insert(ret, {drawIndex = v.drawIndex, f = v.draw, p = {v}})
			end
		end
		return ret
	end,
	
	} return _effect
	
end

return _effect()