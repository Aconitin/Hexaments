_nw = {

	--> Uses: "enet" and "lib/serial"!

	host = nil, --> Holds Server Host Object (enet)!
	clients = {}, --> {adress, peer, name} Holds all clients, indexed by ip!
	
	update = function(dt)
		if _nw.host then
			--> Take care of incoming stuff!
			while true do
				local event = _nw.host:service()
				if event then --> This is where the fun begins!
					_G.statistics.updatesIncoming = _G.statistics.updatesIncoming + 1 --> Statistics!
					--> Split adress into IP and Port!
					local adress = tostring(event.peer) --> Ip of the client that sent the data!
					--> Now see the type of event that happened!
					if event.type == "receive" then --> Received data!
						local data = serialize.deserialized(event.data)
						if data.type == "myname" then --> The name of a client gets sent ASAP after connection!
							_nw.clients[adress].name = data.content --> Set the name of the client!
							_gui.chat.add(_gui.chat.systemColor, "Client " .. adress .. " is now authenticated as " .. _nw.clients[adress].name)
							if sim then
								sim:push({type = "joined", content = {name = data.content, id = adress}}) --> Only when name is clear does the client join the simulation!
							else
								love.errhand("No sim!") --> This is bad!
							end
						else --> Forward the data to the simulation!
							if sim then
								sim:push(data) --> Forward!
							end
						end
					elseif event.type == "connect" then --> Some client wants to connect!
						_gui.chat.add(_gui.chat.systemColor, "Client connected: " .. adress)
						--> Create new client object for server!
						local client = {adress = adress, --> IP:Port pair for indexing!
										peer = event.peer, --> Peer object weil nötig. Punkt!
										name = "Unknown", --> Gets sent in first update!
						}
						if not _nw.clients then
							_nw.clients = {}
						end
						_nw.clients[client.adress] = client --> Add new client to client table!
						event.peer:send(serialize.serialize({type = "yourId", content = client.adress}))
					elseif event.type == "disconnect" then
						_gui.chat.add(_gui.chat.systemColorError, "Client " .. _nw.clients[adress].name .. " disconnected (" .. adress .. ")")
						if sim then
							sim:push({type = "left", content = {name = _nw.clients[adress].name, id = adress}}) --> A player leaves the simulation
						end
						_nw.clients[adress] = nil --> Remove client from client list!
						--> If someone leaves, and is the last player to leave, force a simulation restart!
						if _nw.countClients() == 0 then
							_cmd.startSimulation()
							_gui.chat.add(_gui.chat.systemColor, "Restarted game simulation!")
						end
					end
				else --> If no event, then stop servicing!
					break
				end
			end
			--> Handle outgoing stuff!
			while true do
				local data = sim:fetch()
				if not data then
					break
				else
					_G.statistics.updatesOutgoing = _G.statistics.updatesOutgoing + 1 --> Statistics!
					_nw.host:broadcast(serialize.serialize(data))
				end
			end
		end
	end,
	
	countClients = function()
		if not _nw.clients then
			return 0
		end
		local i = 0
		for c,v in pairs(_nw.clients) do
			i = i + 1
		end
		return i
	end,
	
	getServerLanIP = function() --> Returns a string with the lan server IP:Port!
		local fetch = function()
			local socket = require("socket")
			local s = socket.udp()
			s:setpeername("74.125.115.104",80) --> Google, motherfuckers!
			local ip, _ = s:getsockname()
			if ip then
				return ip
			else
				error("Could not retrieve LAN IP!")
			end
		end
		local worked, msg = pcall(fetch)
		if worked then
			return msg, true
		else
			return "Could not retrieve LAN IP!", false
		end
	end,
	
	getServerIP = function() --> Returns a string with the internal server IP:Port!
		if _nw.host then
			return _nw.host:get_socket_address(), true
		else
			return "Could not retrieve Server-internal IP:Port!", false
		end
	end,

	getExternalIP = function() --> Returns external IP:Port!
		local fetch = function()
			local http = require("socket.http")
			local r,c,h = http.request("http://ipinfo.io/ip") --> Swap if it breaks down!
			if c == 200 then
				return r:sub(0,-2) --> Trim a newline at the end!
			else
				error("Could not retrieve External IP:Port!")
			end		
		end
		local worked, msg = pcall(fetch)
		if worked then
			return msg, true
		else
			return "Could not retrieve External IP!", false
		end
	end,
	
	getStatus = function() --> Returns a description of the status of the server!
		if _nw.host then
			return "Online"
		else
			return "Offline"
		end	
	end,
	
	getServerRuntime = function()
		return love.timer.getTime() - _G.statistics.startTime
	end,
	
	start = function(ip) --> Initializes the networking socket!
		if not _nw.host then
			while not _nw.host do
				_gui.chat.add(_gui.chat.systemColorNeutral, "Trying to host server at " .. ip .. " ...", "txt_console")
				local success = true
				success, _nw.host = pcall(enet.host_create, ip)
				if _nw.host and success then
					_G.statistics.startTime = love.timer.getTime()
					_G.statistics.totalFPS = 0
					_G.statistics.updatesIncoming = 0
					return "Server successfully started! Please make sure your firewall allows access.", true
				else
					_nw.host = nil
					return "Could not start a server. Please try again in a few seconds and check your settings.", false
				end
			end
		else
			return "Server already running!", false
		end
	end,
	
	stop = function()
		if _nw.clients then
			for i,v in pairs(_nw.clients) do
				v.peer:disconnect_now() --> Try to warn all clients of an incoming disconnect!
			end
		end
		_nw.host:flush()
		_nw.host:destroy() --> Kills the host!
		_nw.host = nil --> Kills all reference the host!
		_nw.clients = nil --> Kills all client references!
		collectgarbage() --> Invokes host:__gc(), cutting the servers throat if it is still alive!
	end,
}