_gui = {

	nextUpdateClients = 1,
	updatesIncoming = 0,
	updatesOutgoing = 0,
	updatesOutgoingPerClient = 0,
	
	main = {
		txt_console = {{0, 175, 0, 255}, "[" .. os.date("%H:%M") .. "]  Welcome!"}, --> Initial Console Text, also variable for every console text that will follow (gets appended)!
		txt_clients = {{173, 173, 173, 255}, "No clients connected!"}, --> Initial Console Text, also variable for every console text that will follow (gets appended)!
		list_console = loveframes.Create("list"), --> Grey stuff where the console is hanged up on!
		list_clients = loveframes.Create("list"),
		txtField_console = loveframes.Create("text"), --> Actual displayed console text!
		txtField_clients = loveframes.Create("text"), --> Actual displayed clients text!
		txtIn_main = loveframes.Create("textinput"), --> Console textinput!
	},
	
	chat = {
	
		systemColor = {0, 175, 0, 255},
		systemColorError = {255, 0, 0, 255},
		systemColorNeutral = {173, 173, 173, 255},

		add = function(color, txt, field)
			local field = field or "txt_console"
			table.insert(_gui.main[field], color)
			if field == "txt_console" then
				table.insert(_gui.main[field], " \n " .. "[" .. os.date("%H:%M") .. "]  " .. txt)
			else
				table.insert(_gui.main[field], " \n " .. txt)
			end
			_gui.main.txtField_console:SetText(_gui.main.txt_console)
			_gui.main.txtField_clients:SetText(_gui.main.txt_clients)
		end
	},
	
	update = function(dt) --> Relay!
		_gui.nextUpdateClients = _gui.nextUpdateClients - dt
		if _gui.nextUpdateClients < 0 then
			_gui.nextUpdateClients = 1
			_gui.update60(dt)
		end
	end,
	
	update60 = function(dt) --> Updates client list and title things once per second!
		local nrOfClients = _nw.countClients()
		if _nw.countClients() > 0 then
			_gui.main.txt_clients = {_gui.chat.systemColorNeutral, string.format("Connected Clients [%s]:", nrOfClients)}
			for i,v in pairs(_nw.clients) do
				_gui.chat.add(_gui.chat.systemColor, "", "txt_clients")
				_gui.chat.add(_gui.chat.systemColorNeutral, "Name: " .. v.name, "txt_clients")
				_gui.chat.add(_gui.chat.systemColorNeutral, "IP:Port: " .. v.adress, "txt_clients")
				_gui.chat.add(_gui.chat.systemColorNeutral, "Ping: " .. v.peer:last_round_trip_time(), "txt_clients")
			end
		else
			_gui.main.txtField_clients:SetText{_gui.chat.systemColorNeutral, "No clients connected!"}
		end
		love.window.setTitle("Hexaments Dedicated Server | FPS: " .. love.timer.getFPS() .. " | Status: Server " .. _nw.getStatus())
		_G.statistics.totalFPS = _G.statistics.totalFPS + love.timer.getFPS()
	end,
	
	focus = function()
		_gui.main.txtIn_main:SetFocus(true)
	end,
	
	init = function()
		_gui.resize(love.graphics.getWidth(), love.graphics.getHeight())
		love.graphics.setBackgroundColor(0, 0, 0)
	end,
	
	resize = function(w, h)
		local windowScale = {x = w/1920, y = h/1080}
		local buttonWidth = 200
		local buttonHeight = 25
		local distBetweenBtns = 5
		local font = love.graphics.newFont(12)

		_gui.main.list_clients:SetPos(w-distBetweenBtns-buttonWidth, distBetweenBtns)
		_gui.main.list_clients:SetSize(buttonWidth,  h-1*buttonHeight-2*distBetweenBtns+buttonHeight)
		_gui.main.list_clients:SetState("main")

		_gui.main.list_clients:SetPadding(5)
		_gui.main.list_clients:SetSpacing(5)
		_gui.main.list_clients:AddItem(_gui.main.txtField_clients)
		_gui.main.list_clients:SetAutoScroll(true)
		_gui.main.list_clients:SetDisplayType("vertical")
		
		_gui.main.txtField_clients:SetState("main")
		_gui.main.txtField_clients:SetFont(love.graphics.newFont(13))
		_gui.main.txtField_clients:SetText(_gui.main.txt_clients)

		_gui.main.txtIn_main:SetState("main")
		_gui.main.txtIn_main:SetWidth(w-3*distBetweenBtns-buttonWidth)
		_gui.main.txtIn_main:SetHeight(buttonHeight)
		_gui.main.txtIn_main:SetX(distBetweenBtns)
		_gui.main.txtIn_main:SetY(h-buttonHeight-distBetweenBtns)
		_gui.main.txtIn_main:SetFont(love.graphics.newFont(13))
		_gui.main.txtIn_main.OnEnter = function(object, x, y)
			txt = _gui.main.txtIn_main:GetText()
			_cmd.processCommand(txt)
			_gui.main.txtIn_main:Clear()
			_gui.main.txtIn_main:SetFocus(true)
		end
		
		_gui.main.txtField_console:SetState("main")
		_gui.main.txtField_console:SetWidth(buttonWidth)
		_gui.main.txtField_console:SetHeight(buttonHeight)
		_gui.main.txtField_console:SetFont(love.graphics.newFont(13))
		_gui.main.txtField_console:SetText(_gui.main.txt_console)
		
		_gui.main.list_console:SetState("main")
		_gui.main.list_console:SetPos(distBetweenBtns, distBetweenBtns)
		_gui.main.list_console:SetSize(w-3*distBetweenBtns-buttonWidth, h-1*buttonHeight-3*distBetweenBtns)
		_gui.main.list_console:SetPadding(5)
		_gui.main.list_console:SetSpacing(5)
		_gui.main.list_console:AddItem(_gui.main.txtField_console)
		_gui.main.list_console:SetAutoScroll(true)
		_gui.main.list_console:SetDisplayType("vertical")
	end,
}