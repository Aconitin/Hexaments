_cmd = {

	registeredCommands = { --> For the help section!
						  {"h | start | host <IP:Port>", "Starts the Server."},
						  {"s | stop | shutdown", "Shuts the Server down."},
						  {"r | restart", "Restarts a running Server on the same IP and Port."},
						  {"status | info | stats | statistics", "Display in-depth Server statistics and status."},
						  {"kick <IP:Port | Nickname>", "Kicks a client from the Server."},
						  {"tick <tickrate>", "Displays and/or changes the Server tickrate."},
						  {"fpslimit <FPS>", "Displays and/or changes the Server FPS limit."},
						  {"q | quit", "Terminates the whole Program. If a Server is running, please call \"stop\" first."},
						  {"c | commands | help | cmd", "Shows this List of Commands."},
						 },
	indent = "     ", --> Just a little shortcut!
	recentlyUsed = {"start", "status"}, --> Holds the last 15 recently used commands, accessible via the up and down keys. Start is default cause its often used, same with status!
	recentlyUsedSelected = 0,

	processCommand = function(txt) --> Processes a command that was issued to the server!
		if txt and txt ~= "" then --> Trigger processing only when command is not empty!
			--> Add to recently used command list!
			_cmd.manageRecentlyUsed(txt)
			local cmd = {} --> New command object!
			cmd.txt = string.lower(txt) --> Commands do not care about case!
			cmd.parms = {} --> Parameters table!
			for v in cmd.txt:gmatch("%S+") do --> Fill the parameters table!
				table.insert(cmd.parms, v)
			end
			cmd.cmd = table.remove(cmd.parms, 1) --> Remove first parameter (always the command itself!), put it to actual command variable!
			--> Output a nice text about the issued command!
			_gui.chat.add(_gui.chat.systemColorNeutral, "-->", "txt_console")
			_gui.chat.add(_gui.chat.systemColorNeutral, "Command issued: " .. cmd.cmd, "txt_console")
			if #cmd.parms > 0 then
				_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "Issued Parameters[" .. #cmd.parms .. "]:", "txt_console")
				for i,v in ipairs(cmd.parms) do
					_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. _cmd.indent .. "#" .. i .. ": " .. v, "txt_console")
				end
			end
			--> Now try to run the command!
			if not _cmd[cmd.cmd] then
				_gui.chat.add(_gui.chat.systemColorError, "Unknown Command: \"" .. cmd.cmd .. "\". Please Type \"Commands\" for help.", "txt_console")
			else
				_cmd[cmd.cmd](cmd.parms)
			end
		end
	end,

	--[[
		This section implements all the various commands that one can enter!
	--]]

	quit = function(parms) --> Quits the program!
		_gui.chat.add(_gui.chat.systemColor, "Goodbye!", "txt_console")
		love.event.quit()
	end,

	q = function(p) --> Alias!
		_cmd.quit(p)
	end,

	commands = function(parms) --> Shows command help!
		_gui.chat.add(_gui.chat.systemColor, "Here is a list of all available commands and what they do:", "txt_console")
		for i,v in ipairs(_cmd.registeredCommands) do
			_gui.chat.add(_gui.chat.systemColor, _cmd.indent .. "--> " .. v[1] .. ": " .. v[2], "txt_console")
		end
	end,

	help = function(p) --> Alias!
		_cmd.commands(p)
	end,

	cmd = function(p) --> Alias!
		_cmd.commands(p)
	end,

	c = function(p) --> Alias!
		_cmd.commands(p)
	end,

	start = function(parms) --> Starts the server!
		local goodToGo = false --> For checking parameter validity!
		if #parms == 0 then
			_gui.chat.add(_gui.chat.systemColorNeutral, "No IP:Port parameter given, asuming 127.0.0.1:1337 as IP:Port!", "txt_console")
			parms[1] = "127.0.0.1:1337"
			goodToGo = true
		elseif #parms == 1 then
			goodToGo = true
		elseif #parms > 1 then
			_gui.chat.add(_gui.chat.systemColorError, "Please provide exactly one argument (IP:Port)!", "txt_console")
		end
		if goodToGo then
			local msg, worked = _nw.start(parms[1]) --> Initializes the socket!
			if worked then
				_gui.chat.add(_gui.chat.systemColor, msg, "txt_console") --> Output some fancy "it worked" text!
				_gui.chat.add(_gui.chat.systemColorNeutral, "Relevant Server IPs and Ports you might want to know:", "txt_console")
				_cmd.fetchIPandPortInfo() --> Fetch all info you can find!
				_gui.chat.add(_gui.chat.systemColorNeutral, "Firing up game simulation...", "txt_console")
				_cmd.startSimulation()
				_gui.chat.add(_gui.chat.systemColor, _cmd.indent .. "Game Simulation started!", "txt_console")
			else
				_gui.chat.add(_gui.chat.systemColorError, msg, "txt_console")
			end
		end
	end,

	host = function(p) --> Alias!
		_cmd.start(p)
	end,

	h = function(p) --> Alias!
		_cmd.start(p)
	end,

	--> Little helper to start the sim. This is the only place where the sim is started!
	startSimulation = function()
		--> Seed properly!
		sim = _sim(os.time(), _G.configuration.tick)
		_gui.chat.add(_gui.chat.systemColor, _cmd.indent .. "Seed: " .. sim.seed .. ", SeedFloat: " .. sim.seedFloat .. ", Tickrate: " .. sim.tickrate)
	end,

	--> Helper function for e.g. status and start commands!
	fetchIPandPortInfo = function() --> Prints a bunch of info about used IPs and Ports onto the console, with given base indent!
		--> Retrieve IP:Port the Server is hosted on (intern)!
		local msg, worked = _nw.getServerIP()
		if worked then
			_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "Internal IP:Port: " .. msg, "txt_console")
		else
			_gui.chat.add(_gui.chat.systemColorError, _cmd.indent .. "Internal IP:Port: " .. msg, "txt_console")
		end
		--> Retrieve LAN-intern IP:Port
		local msg, worked = _nw.getServerLanIP()
		if worked then
			_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "LAN IP: " .. msg, "txt_console")
		else
			_gui.chat.add(_gui.chat.systemColorError, _cmd.indent .. "LAN IP: " .. msg, "txt_console")
		end
		--> Retrieve External IP:Port
--		local msg, worked = _nw.getExternalIP()
		if worked then
			_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "External IP: " .. msg, "txt_console")
		else
			_gui.chat.add(_gui.chat.systemColorError, _cmd.indent .. "External IP: " .. msg, "txt_console")
		end
	end,

	shutdown = function(parms) --> Kills the server!
		if _nw.host then
			_gui.chat.add(_gui.chat.systemColorNeutral, "Shutting down Server...", "txt_console")
			_nw.stop()
			sim = nil --> Kill game simulation here and only here!
			_gui.chat.add(_gui.chat.systemColor, "Success!", "txt_console")
		else
			_gui.chat.add(_gui.chat.systemColorError, "There is no Server running at the moment!", "txt_console")
		end
	end,

	stop = function(p) --> Alias!
		_cmd.shutdown(p)
	end,

	s = function(p) --> Alias!
		_cmd.shutdown(p)
	end,

	restart = function(parms)
		if _nw.host then
			_gui.chat.add(_gui.chat.systemColorNeutral, "Restarting Server...", "txt_console")
			local ipPort = _nw.host:get_socket_address()
			_cmd.shutdown(parms)
			_cmd.start({ipPort})
		else
			_gui.chat.add(_gui.chat.systemColorError, "There is no Server running at the moment!", "txt_console")
		end
	end,

	r = function(p) --> Alias!
		_cmd.restart(p)
	end,

	status = function(parms) --> Displays server statistics!
		if _nw.host then
			_gui.chat.add(_gui.chat.systemColor, "Server online, displaying status:", "txt_console")
			_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "-->", "txt_console")
			_cmd.fetchIPandPortInfo()
			_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "-->", "txt_console")
			_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "Server Runtime: " .. string.format("%.4f seconds", _nw.getServerRuntime()), "txt_console")
			_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "Current Server FPS: " .. love.timer.getFPS() .. " (limited to " .. _G.configuration.maxFPS .. " FPS)", "txt_console")
			_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "Average Server FPS: " .. _G.statistics.totalFPS/_nw.getServerRuntime(), "txt_console")
			_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "-->", "txt_console")
			_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "Server tickrate: " .. _G.configuration.tick, "txt_console")
			_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "Total Sent Updates: " .. _G.statistics.updatesOutgoing, "txt_console")
			_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "Total Received Updates: " .. _G.statistics.updatesIncoming, "txt_console")
			_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "Current state of sim.inc and sim.out: " .. #sim.inc .. "|" .. #sim.out, "txt_console")
			_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "-->", "txt_console")
			_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "Total Sent Data: " .. _nw.host:total_sent_data()/1000000 .. " MB", "txt_console")
			_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "Average Sent Data: " .. _nw.host:total_sent_data()/1000000/_nw.getServerRuntime() .. " MB/s", "txt_console")
			_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "Total Received Data: " .. _nw.host:total_received_data()/1000000 .. " MB", "txt_console")
			_gui.chat.add(_gui.chat.systemColorNeutral, _cmd.indent .. "Average Received Data: " .. _nw.host:total_received_data()/1000000/_nw.getServerRuntime() .. " MB/s", "txt_console")
		else
			_gui.chat.add(_gui.chat.systemColorError, "No Server Online!", "txt_console")
		end
	end,

	stats = function(p) --> Alias!
		_cmd.status(p)
	end,

	statistics = function(p) --> Alias!
		_cmd.status(p)
	end,

	info = function(p) --> Alias!
		_cmd.status(p)
	end,

	tick = function(parameters)
		if #parameters == 1 then
			if tonumber(parameters[1]) then
				_G.configuration.tick = parameters[1]
				_gui.chat.add(_gui.chat.systemColor, "Changed Server tickrate to " .. _G.configuration.tick, "txt_console")
			else
				_gui.chat.add(_gui.chat.systemColorError, "Tickrate must be a number (number of gamestate updates per second), default 60!", "txt_console")
			end
		elseif #parameters == 0 then
			_gui.chat.add(_gui.chat.systemColor, "Current Server tickrate is " .. _G.configuration.tick .. ".", "txt_console")
		else
			_gui.chat.add(_gui.chat.systemColorError, "Please provide exactly one argument!", "txt_console")
		end
	end,

	fpslimit = function(parameters)
		if #parameters == 1 then
			if tonumber(parameters[1]) then
				_G.configuration.maxFPS = parameters[1]
				_gui.chat.add(_gui.chat.systemColor, "Changed Server FPS limit to " .. _G.configuration.maxFPS, "txt_console")
			else
				_gui.chat.add(_gui.chat.systemColorError, "FPS limit must be a number (default 120)!", "txt_console")
			end
		elseif #parameters == 0 then
			_gui.chat.add(_gui.chat.systemColor, "Current Server FPS limit is " .. _G.configuration.maxFPS .. ".", "txt_console")
		else
			_gui.chat.add(_gui.chat.systemColorError, "Please provide exactly one argument!", "txt_console")
		end
	end,

	kick = function(parameters)
		if #parameters == 1 then
			if parameters[1] == "all" then
				if _nw.countClients() > 0 then
					for i,v in pairs(_nw.clients) do
						v.peer:disconnect_now() --> Bye, bye!
						_nw.clients[v.adress] = nil
					end
					_nw.clients = nil
					_gui.chat.add(_gui.chat.systemColor, "All clients were kicked from the Server!", "txt_console")
					--> If someone leaves, and is the last player to leave, force a simulation restart!
					if _nw.countClients() == 0 then
						_cmd.startSimulation()
						_gui.chat.add(_gui.chat.systemColor, "Restarted game simulation!")
					end
				else
					_gui.chat.add(_gui.chat.systemColorError, "There is nobody to kick!", "txt_console")
				end
			else
				local kicked = 0
				for i,v in pairs(_nw.clients) do
					if string.lower(v.name) == parameters[1] or v.adress == parameters[1] then --> Kick that motherfucker!
						_gui.chat.add(_gui.chat.systemColor, v.name .. " was kicked from the Server!", "txt_console")
						v.peer:disconnect_now() --> Bye, bye!
						_nw.clients[v.adress] = nil
						kicked = kicked + 1
					end
				end
				if kicked == 0 then
					_gui.chat.add(_gui.chat.systemColorError, "Client \"" .. parameters[1] .. "\" not found!", "txt_console")
				else
					--> If someone leaves, and is the last player to leave, force a simulation restart!
					if _nw.countClients() == 0 then
						_cmd.startSimulation()
						_gui.chat.add(_gui.chat.systemColor, "Restarted game simulation!")
					end
				end
			end
		else
			_gui.chat.add(_gui.chat.systemColorError, "Please provide exactly one argument!", "txt_console")
		end
	end,

	--[[
		This section implements the functionality that the 15 last used commands can be quickly put into the command bar with the up and down arrows!
	--]]

	manageRecentlyUsed = function(txt)
		table.insert(_cmd.recentlyUsed, 1, txt)
		_cmd.recentlyUsedSelected = 0
		for i,v in ipairs(_cmd.recentlyUsed) do
			if i ~= 1 and v == txt then
				table.remove(_cmd.recentlyUsed, i)
			end
		end
		while #_cmd.recentlyUsed > 15 do --> Nur die letzten 15 commands speichern!
			table.remove(_cmd.recentlyUsed, #_cmd.recentlyUsed)
		end
	end,

	toggleRecentlyUsed = function(key)
		if key == "up" then
			_cmd.recentlyUsedSelected = _cmd.recentlyUsedSelected + 1
			if _cmd.recentlyUsedSelected > #_cmd.recentlyUsed then
				_cmd.recentlyUsedSelected = #_cmd.recentlyUsed
			end
			_cmd.selectRecentlyUsed()
		elseif key == "down" then
			_cmd.recentlyUsedSelected = _cmd.recentlyUsedSelected - 1
			if _cmd.recentlyUsedSelected < 1 then
				_cmd.recentlyUsedSelected = 1
			end
			_cmd.selectRecentlyUsed()
		end
	end,

	selectRecentlyUsed = function()
		_gui.main.txtIn_main:Clear()
		_gui.main.txtIn_main:SetFocus(true)
		local cmd = _cmd.recentlyUsed[_cmd.recentlyUsedSelected]
		_gui.main.txtIn_main:SetText(cmd)
		_gui.main.txtIn_main:MoveIndicator(string.len(cmd), true)
	end,

	--[[
	--]]
}
