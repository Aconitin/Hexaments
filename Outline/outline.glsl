vec4 resultCol;
extern vec2 stepSize;

vec4 effect( vec4 col, Image texture, vec2 texturePos, vec2 screenPos )
{
	// get color of pixels:
	number alpha = texture2D( texture, texturePos ).a;
	number ret = 4*alpha;
	ret -= texture2D( texture, texturePos + vec2( stepSize.x, 0.0f ) ).a;
	ret -= texture2D( texture, texturePos + vec2( -stepSize.x, 0.0f ) ).a;
	ret -= texture2D( texture, texturePos + vec2( 0.0f, stepSize.y ) ).a;
	ret -= texture2D( texture, texturePos + vec2( 0.0f, -stepSize.y ) ).a;

 vec4 c = Texel(texture, texturePos);

	// calculate resulting color
//	resultCol = texture2D( texture, texturePos );
	resultCol = vec4( 1.0f, 1.0f, 1.0f, ret);
	// return color for current pixel
	return resultCol * col;
}
