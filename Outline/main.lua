-- Tutorial Framework
-- By Germanunkol, April 2014
-- Code release under the "Do What The Fuck You Want To Public License" (see http://www.wtfpl.net for details )

-- Image files are from here (Skeleton):
-- http://opengameart.org/content/lpc-skeleton
-- And here (Spear man):
-- http://opengameart.org/content/spear-walk
-- Both are released under Creative Common Licenses, make sure to visit the Links for more details before using the images in your own projects.
-- Thanks to the authors!

Character = require("character")
Shaders = require("shaders")

local chars = {}

function love.load()


	-- initialize character class:
	--Character:init()
	
	-- create and remember two characters - a skeleton and a 
--	chars[1] = Character:new("skeleton")
	--chars[2] = Character:new("spearguy")

--	chars[1]:setPosition( love.graphics.getWidth()/3-34, love.graphics.getHeight()/2-40 )
--	chars[2]:setPosition( 2*love.graphics.getWidth()/3-34, love.graphics.getHeight()/2-40 )

	love.graphics.setFont( love.graphics.newFont(14) )

	-- To add a new "Shader", use a line similar to the following.
	-- This will not really create a shader, only display a text at the top which can be used
	-- to disable and enable shaders. In order for this to work, functions using shaders MUST
	-- poll Shader:isEnabled( "outline" ) or similar before using shaders.
	Shaders:addShader( "plain", "1" )
	Shaders:addShader( "outline thick", "2" )
	Shaders:addShader( "outline thin", "3" )
	--Shaders:addShader( "gaussian", "g" )
	
	particleSystemCanvas = love.graphics.newCanvas(16, 16)
	particleSystemCanvas:renderTo(function()
		love.graphics.clear(0, 0, 0, 0)
		love.graphics.setColor(255, 255, 255, 255)
		love.graphics.rectangle("fill", 4, 4, 8, 8)
	end)

	ps = love.graphics.newParticleSystem(particleSystemCanvas, 1000)
	ps:setAreaSpread("uniform", 0, 0)
	ps:setColors(255, 255, 255, 0, 255, 255, 255, 100, 255, 255, 255, 200, 255, 255, 255, 200, 0, 255, 0, 200, 255, 255, 255, 200, 255, 255, 255, 200, 255, 255, 255, 0)
	ps:setDirection(math.rad(0))
	ps:setEmissionRate(100)
	ps:setEmitterLifetime(-1)
	ps:setInsertMode("bottom")
	ps:setParticleLifetime(2, 2)
	ps:setRadialAcceleration(-100, -100)
	ps:setRotation(math.rad(0), math.rad(360))
	ps:setSizes(1, 1, 0.5, 0.25, 0)
	ps:setSizeVariation(1)
	ps:setSpeed(60, 80)
	ps:setSpin(math.rad(0), math.rad(360))
	ps:setSpinVariation(1)
	ps:setSpread(math.rad(360))
	ps:setTangentialAcceleration(40, 40)
	ps:start()
	
	
	shader = love.graphics.newShader( "outline.glsl" )
	weird = true
end

function love.update( dt )
--	chars[1]:update( dt )
--	chars[2]:update( dt )
ps:update(dt)
end

function love.draw()
	
love.graphics.setColor(255, 255, 255, 255)

	shader:send( "stepSize", {1/8, 1/8})
	love.graphics.draw(ps, 100, 200)
	love.graphics.draw(ps, 500, 200)
	love.graphics.setColor(255, 255, 255, 255)
	love.graphics.setShader( shader )
	love.graphics.draw(ps, 300, 200)
	love.graphics.draw(ps, 500, 200)
--	love.graphics.draw(particleSystemCanvas, 250, 250)
	love.graphics.setShader( )
if weird then
love.graphics.setColor(255, 255, 255, 100)
love.graphics.draw(ps, 300, 400)
love.graphics.setColor(255, 255, 255, 255)
	love.graphics.setShader( shader )
	
	love.graphics.draw(ps, 300, 400)
--	love.graphics.draw(particleSystemCanvas, 250, 250)
	love.graphics.setShader( )
	else

		love.graphics.draw(ps, 300, 400)
	
	
	
	end
end

function love.keypressed( key )
	if key == "escape" then
		love.event.quit()
	end
	weird = not weird
end
