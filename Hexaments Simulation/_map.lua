local _map = {} --> Map superclass!

--> Adds a column to the map, to the right or left!
function _map.addColumn(sim, leftOrRight)
	local self = sim._map
	if leftOrRight == "right" then
		sim.highestColumnIndex = sim.highestColumnIndex + 1
		sim.columns = sim.columns + 1
		for i=sim.lowestRowIndex,sim.highestRowIndex,1 do
			self.addHexagon(sim, sim.highestColumnIndex, i)
		end
	elseif leftOrRight == "left" then
		sim.lowestColumnIndex = sim.lowestColumnIndex - 1
		sim.columns = sim.columns + 1
		for i=sim.lowestRowIndex,sim.highestRowIndex,1 do
			self.addHexagon(sim, sim.lowestColumnIndex, i)
		end
	end
end

--> Adds a row to the map, up or down!
function _map.addRow(sim, upOrDown)
	local self = sim._map
	if upOrDown == "down" then
		sim.highestRowIndex = sim.highestRowIndex + 1
		sim.rows = sim.rows + 1
		for i=sim.lowestColumnIndex,sim.highestColumnIndex,1 do
			self.addHexagon(sim, i, sim.highestRowIndex)
		end
	elseif upOrDown == "up" then
		sim.lowestRowIndex = sim.lowestRowIndex - 1
		sim.rows = sim.rows + 1
		for i=sim.lowestColumnIndex,sim.highestColumnIndex,1 do
			self.addHexagon(sim, i, sim.lowestRowIndex)
		end
	end
end

--> Returns a new hexagon object with values based on the given x/y position (index) of the hexagon. w and h are full width and height of hexagon!
function _map.addHexagon(sim, x, y)
	local self = sim._map
	
	--> Create hexagon values!
	local hexX, hexY = self.getHexagonCoordsByIndex(x, y, sim.hexagonWidth, sim.hexagonHeight) --> Generate the new hexagon's center x and y coordinates!
	local elevation = self.getHexagonElevation(x, y, sim.seedFloat)
	local biome = self.getBiomeFromElevation(elevation)
	local newHex = sim._hexagon.create(hexX, hexY, sim.hexagonWidth, sim.hexagonHeight, elevation, biome) --> Create hexagon object!
	
	if not sim.map.hexagons then
		sim.map.hexagons = {} --> Holds all the map's hexagon objects!
	end
	
	if not sim.map.hexagons[x] then
		sim.map.hexagons[x] = {}
	end
	sim.map.hexagons[x][y] = newHex

	--> Update map size!
	sim.mapWidth, sim.mapHeight = self.getSize(sim)

end

--> Fetches largest map size in pixels!
function _map.getSize(sim)
	local w = sim.columns * 0.75 * sim.hexagonWidth + 0.25 * sim.hexagonWidth
	local h = (sim.rows + 0.5) * sim.hexagonHeight
	return w, h
end

--> Gets basic biome from elevation!
function _map.getBiomeFromElevation(e)
	if e < 0.06 then
		return "water"
	elseif e < 0.08 then
		return "beach"
	elseif e < 0.63 then
		return "grass"
	else
		return "mountains"
	end
end

--> Returns hexagon x/y center position by x/y index and size!
function _map.getHexagonCoordsByIndex(posX, posY, w, h)
	return (posX - 1) * 1.5 * (w/2) + (w/2), 2 * ((posY - 1) * (h/2) + ((posX + 1) % 2) * (h/2) * 0.5 + 0.5 * (h/2)) --> Shut up. It works!
end

--> Returns a single hexagon's elevation!
function _map.getHexagonElevation(x, y, seedFloat)
	local frequency, power = 0.05, 2 --> Some parameters for the heighmap!
	local elevation = love.math.noise(frequency * (x + seedFloat), frequency * (y + seedFloat)) --> Generate deterministic, seeded, random, noised elevation value for the hexagon!
						+ 0.5 * love.math.noise(frequency * 2 * (x + seedFloat), frequency * 2 * (y + seedFloat))
						+ 0.25 * love.math.noise(frequency * 4 * (x + seedFloat), frequency * 4 * (y + seedFloat))
	return math.pow(elevation/1.75, power)
end

--> Adds some part to the map and returns a point on that new part that is good for a new player as spawnpoint!
function _map.enlarge(sim, s) --> Takes s, which is the number of rows or columns to add for one new player!
	local rlup = sim.gen:random(1, 2) --> Determines wether the columns/rows are added right/left or up/down
	local f
	local str
	if sim.columns < sim.rows then
		f = sim._map.addColumn
		if rlup == 1 then
			str = "right"
		else
			str = "left"
		end
	else
		f = sim._map.addRow
		if rlup == 1 then
			str = "up"
		else
			str = "down"
		end
	end
	for i=1,s,1 do
		f(sim, str)
	end
	local spawnPointX, spawnPointY
	local a, b
	if str == "right" then
		a = sim.highestColumnIndex - 3
		b = math.ceil(sim.highestRowIndex - 0.5 * sim.rows)
	elseif str == "left" then
		a = sim.lowestColumnIndex + 3
		b = math.ceil(sim.highestRowIndex - 0.5 * sim.rows)
	elseif str == "up" then
		a = math.ceil(sim.highestColumnIndex - 0.5 * sim.columns)
		b = sim.lowestRowIndex + 3
	elseif str == "down" then
		a = math.ceil(sim.highestColumnIndex - 0.5 * sim.columns)
		b = sim.highestRowIndex - 3
	end
	local sx, sy = sim._map.getHexagonCoordsByIndex(a, b, sim.hexagonWidth, sim.hexagonHeight)
	return sx, sy, a, b
end

return _map

