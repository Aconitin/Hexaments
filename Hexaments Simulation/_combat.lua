local _combat = function() --> Manages all fights between units!

	local self = {}

	self.instances = {} --> Holds ongoing battles!
	
	function self:update(dt, sim) --> Returns function to apply and deapply this update!
	
		local deapplyFunctions = {}
	
		--> Try to find new combats first!
		local deapply = function() end --> Empty deapply function!
		for _,u1 in ipairs(sim.units) do
			if u1.maxTargets > self:countAttacks(u1) then --> This unit is allowed to attack somebody!
				for __,u2 in ipairs(sim.units) do
					if u1 ~= u2 then --> Don't attack yourself!
						if math.dist(u1.x, u1.y, u2.x, u2.y) <= u1.range then
							local fight = {attacker = u1, attacked = u2}
							table.insert(self.instances, fight)
							table.insert(deapplyFunctions, function() table.remove(self.instances) end)
							break --> Only one new battle per unit per update!
						end
					end
				end
			end
		end
	
		--> Update ongoing battles!
		for i=#self.instances,1,-1 do

			local v = self.instances[i]
		
			--> Check if still in range and nobody is dead yet!
			if v.attacker.dead or
			   v.attacked.dead or 
			   math.dist(v.attacker.x, v.attacker.y, v.attacked.x, v.attacked.y) > v.attacker.range then
				local removed = table.remove(self.instances, i)
				table.insert(deapplyFunctions, function() table.insert(self.instances, i, removed) end)
			else

				--> Update hp of attacked unit!
				local oldHp = v.attacked.hp
				v.attacked.hp = math.clamp(0, v.attacked.hp - v.attacker.dps * dt, 1)
				--> Create deapply for that!
				table.insert(deapplyFunctions, {parameters = {attacked = v.attacked,
															  hp = oldHp
												},
												func = function(p)
													p.attacked.hp = p.hp
												end
				})
				--> Done!
			end
			
		end
	
		return deapplyFunctions
	
	
	end
	
	--> Returns number of units that this unit attacks!
	function self:countAttacks(unit)
		i = 0
		for _,v in ipairs(self.instances) do
			if v.attacker == unit then
				i = i + 1
			end	
		end
		return i
	end	
	
	return self
	
end

return _combat