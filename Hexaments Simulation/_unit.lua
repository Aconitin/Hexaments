local _unit = {}

function _unit.create(p)

	local self = {} --> New unit!

	local p = p or {} --> Parameters!
	self.type = p.type or "wind" --> unit type (element)!
	self.x = p.x or 0 --> X position!
	self.y = p.y or 0 --> Y position!
	self.s = p.s or 60 --> Speed!
	self.hexX = p.hexX --> Hexagon x index where the unit is!
	self.hexY = p.hexY --> Hexagon y index where the unit is!
	self.owner = p.owner --> Owner id of unit. Holds a reference to sim.players[id]!

	self.hp = p.hp or 1/12 --> Magnitude (Size, strength, how many resources it has). Also represents health. Percentage (between 0 and 1)!
	self.range = p.range or 1.5 * sim.hexagonWidth * math.cos(math.rad(30)) --> Default range is about 1.5 hexagons!
	self.maxTargets = p.maxTargets or 1
	self.dps = p.dps or 0.1 --> Damage per second!

	self.waypoints = p.waypoints or {} --> Where it wants to move!
	self.orders = p.orders or {} --> Contains a list of (movement/whatever)-orders. An order contains a type and target field!
	self.status = p.status or "idle" --> What the unit is doing at the moment!

	--> Update!
	function self:update(dt)

		local deapplyFunctions = {}

		self.status = "idle"

		--> Process orders!
		if #self.orders ~= 0 and #self.waypoints == 0 then
			local newOrder = table.remove(self.orders, 1) --> Take new order from order stack!
			if newOrder.type == "move" then --> A move order has the hexagon index where to move to as content field!
				self:setMovementGoal(newOrder.target) --> Invokes A* to find all waypoints!
				--> Create deapply for that!
				table.insert(deapplyFunctions, {parameters = {self = self, removedOrder = newOrder
												},
												func = function(p)
													p.self.waypoints = {}
													table.insert(p.self.orders, 1, p.removedOrder)
												end
				})
				--> Done!
			end
		end

		--> Update waypoints!
		if #self.waypoints ~= 0 then --> Move towards waypoints!
			self.status = "moving"
			local t = self.waypoints[1]
			local angle = math.angle(self.x, self.y, t.gcx, t.gcy)
			self.x, self.y = self.x + self.s * dt * math.cos(angle), self.y + self.s * dt * math.sin(angle)
			--> Create deapply for that!
			table.insert(deapplyFunctions, {parameters = {self = self,
														  dx = self.s * dt * math.cos(angle),
														  dy = self.s * dt * math.sin(angle),
											},
											func = function(p)
												p.self.x, p.self.y = p.self.x - p.dx, p.self.y - p.dy
											end
			})
			--> Done!
			if not t.onTargetHexagon and math.dist(self.x, self.y, t.gcx, t.gcy) <= 0.5 * sim.hexagonWidth * math.cos(math.rad(30)) then --> Unit crosses hexagon border!
				if self:mayEnter(t.gix, t.giy) then --> Double check that the unit is actually allowed to enter the hexagon. If yes, proceed!
					--> Create deapply for that!
					local oldHexX, oldHexY = self.hexX, self.hexY

					table.insert(deapplyFunctions, {parameters = {self = self, oldHexX = oldHexX, oldHexY = oldHexY,
																  waypoints = self.waypoints
													},
													func = function(p)
														p.self.hexX, p.self.hexY = p.oldHexX, p.oldHexY
														p.waypoints[1].onTargetHexagon = false
													end
					})
					--> Done!
					self.hexX, self.hexY = t.gix, t.giy
					t.onTargetHexagon = true
				else
					self:clearOrders() --> If it's not allowed to enter, stand right on the crossing line and discard all orders!
					--> Create deapply for that!
					local oldOrders, oldWaypoints, self = self.orders, self.waypoints, self

					table.insert(deapplyFunctions, {parameters = {self = self, oldOrders = oldOrders, oldWaypoints = oldWaypoints
													},
													func = function(p)
														p.self.orders, p.self.waypoints = p.oldOrders, p.oldWaypoints
													end
					})
					--> Done!
				end
			end
			if math.dist(self.x, self.y, t.gcx, t.gcy) <= self.s * dt then --> Unit arrives on center!
					--> Create deapply for that!
					local oldX, oldY, self = self.x, self.y, self
					local wp = self.waypoints[1]
					table.insert(deapplyFunctions, {parameters = {self = self, oldX = oldX, oldY = oldY,
																  wp = wp
													},
													func = function(p)
														p.self.x, p.self.y = p.oldX, p.oldY
														table.insert(p.self.waypoints, 1, p.wp)
													end
					})
					--> Done!
				self.x, self.y = t.gcx, t.gcy
				table.remove(self.waypoints, 1)
			end
		end


        if self.status == "idle" then
            local neighbours = sim._hexagon.getNeighbours(self.hexX, self.hexY, true)
            local i = 0
            for _,v in pairs(neighbours) do
                local hex = sim.map.hexagons[v.x][v.y]
                i = i + hex.elevation
            end
            if i ~= 0 then
                self.status = "collecting"
                self.hp = math.clamp(0, self.hp + (i * dt * 0.05), 1)
                print(self.hp)
            end
        end

		return deapplyFunctions

	end

	--> Returns true if a unit may enter a specified hexagon. A Unit cannot enter e.g. a hexagon which holds an enemy unit!
	function self:mayEnter(x, y)
		local hex = sim.map.hexagons[x][y]

		--> Check if hexagon is out of bounds (the one covered by fog is also out of bounds!
		if x < sim.lowestColumnIndex+1 or y < sim.lowestRowIndex+1 or x > sim.highestColumnIndex-1 or y > sim.highestRowIndex-1 then
			return false
		end

		--> Check for enemy units on that hexagon!
		for _,v in ipairs(sim.units) do --> This is not a big problem because there will not be a lot of units!
			if v:isOn(x, y) and v.owner.id ~= self.owner.id then
				return false
			end
		end

		return true
	end

	--> Order to set specified target hexagon to new movement goal (target). Finds a path to the target and applies that to waypoints. Uses A*!
	function self:setMovementGoal(target)
		local map = sim.map

		--> Define start and target node (hexagon) for the pathfinding algorithm (indexes, not coords)!
		local start = {x = self.hexX, y = self.hexY, node = sim.map.hexagons[self.hexX][self.hexY]}
		local target = {x = target.x, y = target.y, node = sim.map.hexagons[target.x][target.y]}

		--> Define a cost estimate function returning the estimated costs to get from a to b based on some distance function/heuristics!
		local heuristicCostEstimate = function(a, b)
			ax, ay = sim._map.getHexagonCoordsByIndex(a.x, a.y, sim.hexagonWidth, sim.hexagonHeight)
			bx, by = sim._map.getHexagonCoordsByIndex(b.x, b.y, sim.hexagonWidth, sim.hexagonHeight)
			return (math.abs(ax - bx) + math.abs(ay - by)) / 2
		end

		--> This defines all valid nodes and differs between unit types. A flying unit has all nodes as neighbours, a ground e.g. cannot walk over mountains!
		local validNeighbours = function(x, y, withSelf)
			local neighbours = sim._hexagon.getNeighbours(x, y, withSelf)
			for i=#neighbours,1,-1 do
				if not self:mayEnter(neighbours[i].x, neighbours[i].y) then
					table.remove(neighbours, i)
				end
			end
			return neighbours
		end

		--> Defines a distance function returning the distance between a and b!
		local distance = function(ax, ay, bx, by)
			return (math.abs(ax - bx) + math.abs(ay - by)) / 2
		end

		--> Get the A* path from start to target!
		local path = sim._aStar:aStar(map, start, target, heuristicCostEstimate, validNeighbours, distance)

		--> If there is a path, then for each node in the path, apply the node correctly to the waypoints table of that unit!
		if path then
			for i=#path,1,-1 do
				local goal = path[i]
				local from = path[i+1] or goal
				local goalNode, fromNode = sim.map.hexagons[goal.x][goal.y], sim.map.hexagons[from.x][from.y]
				local gcx, gcy = sim._map.getHexagonCoordsByIndex(goal.x, goal.y, sim.hexagonWidth, sim.hexagonHeight)
				local fcx, fcy = sim._map.getHexagonCoordsByIndex(from.x, from.y, sim.hexagonWidth, sim.hexagonHeight)
				--> Goal Coordinates X/Y, Goal Index X/Y, From Coordinates X/Y, From Index X/Y. Bool = true when unit is on that hexagon. Includes mountain height!
				table.insert(self.waypoints, {gcx = gcx, gcy = gcy - goalNode.height, gix = goal.x, giy = goal.y, onTargetHexagon = false, fcx = fcx, fcy = fcy - fromNode.height, fix = from.x, fiy = from.y})
			end
		end
	end

	--> Gets called when this unit receives some kind of order!
	function self:order(p)
		--[[ p is the parameters table for the order. Depending on the order, some parameters are variable. Fixed are:

				p.unitIndex = index of the unit which received the order in the sim.units table
				p.type = string, type of the order, e.g. "move"
				p.additional = boolean, wether to add the order to the order stack or make it an exclusive order (deleting the stack first)

				For "move" order:
				p.target = {x, y} contains the hexagon index of the hexagon where to move to
		--]]
		if not p.additional then --> If this is a non-shift (exclusive) order, then reset the order stack first and all waypoints!
			self:clearOrders()
		end
		table.insert(self.orders, {type = p.type, target = p.target})
	end

	--> Clears all orders and waypoints from this unit. Basically makes it stand still. Useful when an exclusive order arrives!
	function self:clearOrders()
		self.orders = {}
		self.waypoints = {}
	end

	--> Returns true if unit is on the specified x/y index hexagon field!
	function self:isOn(x, y)
		return ((x == self.hexX) and (y == self.hexY))
	end

	return self

end

return _unit
