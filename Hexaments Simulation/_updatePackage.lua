local _updatePackage = function()

	--> Create a new package!
	local newPackage = {applyFunctions = {}, --> Holds functions to be called to apply this update!
						deapplyFunctions = {}, --> Holds functions that revert this update, in order!
						step = sim.step, --> This holds the timestep at which the update held in this package is supposed to be applied!
	}
	--[[
		One apply or deapply function table content holds:
			{parameters = {self = self, removedOrder = newOrder
			},
			func = function(p)
				p.self.waypoints = {}
				table.insert(p.self.orders, 1, p.removedOrder)
			end
	--]]
	
	--> This applies all updates held in this package!
	function newPackage:apply()
		
		--> Apply all steps to be taken in this update!
		for _, t in ipairs(self.applyFunctions) do
			t.func(t.parameters)
		end
print("applied " .. self.step)		
	end	
	
	--> When called, this function de-applies the update held in the package!
	function newPackage:deapply()
	
		--> De-Apply all steps taken in this update, in reverse order!
		for i=#self.deapplyFunctions,1,-1 do
			local t = self.deapplyFunctions[i]
			t.func(t.parameters)
		end
		
print("deapplied " .. self.step)
	end	
	
	--> Extends the apply or deapply functions with a given list. applyOrDeapply = apply when true, deapply when false!
	function newPackage:extend(list, applyOrDeapply)
		for _,f in ipairs(list) do
			if applyOrDeapply then
				table.insert(self.applyFunctions, f)
			else
				table.insert(self.deapplyFunctions, f)
			end
		end
	end
	
	return newPackage
	
end

return _updatePackage