local _hexagon = {} --> Holds the functions for the simulation's hexagon objects!

function _hexagon.create(x, y, w, h, elevation, biome) --> Creates and returns a new hexagon!

	local ret = {} --> New hexagon!
	
	ret.x = x or 0 --> x coordinates on the screen in pixels, center of hexagon!
	ret.y = y or 0 --> y coordinates on the screen in pixels, center of hexagon!
	ret.w = w or 0 --> Hexagon total width!
	ret.h = h or 0 --> Hexagon total height!

	ret.elevation = elevation or 0 --> Hexagon elevation!
	ret.biome = biome or "nil" --> Biome (resulting from elevation)!
	ret.height = 0
	
	--> Calculate the actual height of the hexagon in pixels if the hexagon is a mountain. This is a nice shortcut, otherwise the height has to be re-calculated all the time!
	if ret.biome == "mountains" then
		local height = math.floor(ret.elevation * 10^2 + 0.5) / 10^2
		if height < 0.66 then
			height = 0.15
		elseif height < 0.71 then
			height = 0.3
		elseif height < 0.78 then
			height = 0.4
		elseif height < 1 then
			height = 0.5
		end
		ret.height = height * h --> Final height offset in pixels!
	end
	
	--> Points of the hexagon. Point 1 is the left center point, then going clockwise!
	ret.points = {
				 {x = ret.x - 0.5 * w,	--> p1x
				  y = ret.y},			--> p1y
				 {x = ret.x - 0.25 * w,	--> p2x
				  y = ret.y - 0.5 * h},	--> p2y
				 {x = ret.x + 0.25 * w,	--> p3x
				  y = ret.y - 0.5 * h},	--> p3y
				 {x = ret.x + 0.5 * w,	--> p4x
				  y = ret.y},			--> p4y
				 {x = ret.x + 0.25 * w,	--> p5x
				  y = ret.y + 0.5 * h},	--> p5y
				 {x = ret.x - 0.25 * w,	--> p6x
				  y = ret.y + 0.5 * h}	--> p6y
	}
	
	--> For drawing this hexagon's border fast. With height calculated in!
	ret.borderPointsWithHeight = {}
	for i,v in ipairs(ret.points) do
		table.insert(ret.borderPointsWithHeight, v.x)
		table.insert(ret.borderPointsWithHeight, v.y - ret.height)
	end
	table.insert(ret.borderPointsWithHeight, ret.points[1].x)
	table.insert(ret.borderPointsWithHeight, ret.points[1].y - ret.height)

	return ret
end

--> Return all neighbours of a given hexagon coordinate!
function _hexagon.getNeighbours(x, y, withSelf)
	local neighbours
	if x % 2 == 1 then
		neighbours = {
				{x = x, y = y-1},
				{x = x, y = y+1},
				{x = x-1, y = y-1},
				{x = x-1, y = y},
				{x = x+1, y = y-1},
				{x = x+1, y = y},
		}
	else
		neighbours = {
				{x = x, y = y-1},
				{x = x, y = y+1},
				{x = x-1, y = y},
				{x = x-1, y = y+1},
				{x = x+1, y = y},
				{x = x+1, y = y+1},
		}
	end
	if withSelf then --> Include the middle tile, too, not only the neighbours!
		table.insert(neighbours, 1, {x = x, y = y})
	end
	for i=#neighbours,1,-1 do
		local v = neighbours[i]
		if v.x < sim.lowestColumnIndex or v.y < sim.lowestRowIndex or v.x > sim.highestColumnIndex or v.y > sim.highestRowIndex then
			table.remove(neighbours, i)
		end
	end
	return neighbours
end

return _hexagon